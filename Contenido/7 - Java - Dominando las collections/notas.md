# **Java - Dominando las collections**

## **Trabajando con ArrayList**

### **Cómo adicionar elementos a un ArrayList**

Si tenemos una lista de tipo String llamada alumnos, ¿cuál sería el método y valor para adicionar de forma correcta a la lista sin errores de compilación?

Rta.

```java
alumnos.add(“Pepito”);
```

### **Recorrer una lista de String**

Si tenemos una lista de String llamada alumnos, ¿cómo recorremos todos los valores de esa lista?

Rta.

```java
for ( String alumno :  alumnos) { /*---------*/ }
```

### **Ordenar una lista de String**

Queremos imprimir de forma descendente una lista de String llamada alumnos ¿cuál sería la forma de poder hacerlo?

```java
Collections.sort(alumnos, Collections.reverseOrder());
```

---

## **Listas de objetos**

### **Adicionar un Objeto a una lista**

Queremos añadir un nuevo curso a nuestra lista de Curso ¿cuál sería la forma de poder hacerlo?

```java
Curso curso1 = new Curso("Geometria",30) ;
ArrayList<Curso> cursos = new ArrayList<>();
cursos.add(curso1);
```

Rta.

```java
cursos.add(new Curso("Aritmética",20));
```

### **Ordenando listas**

Queremos ordenar una lista descendentemente por el tiempo de duración de _Cursos_ con atributos que se llaman _nombre_ y _tiempo_ ¿Cuál sería la forma de poder hacerlo?

```java
Curso curso1 = new Curso("PHP",30) ;
Curso curso2 = new Curso("Ruby",10) ;
Curso curso3 = new Curso("JavaScript",20) ;
Curso curso4 = new Curso("Java",50) ;

ArrayList<Curso> cursos = new ArrayList<>();
cursos.add(curso1);
cursos.add(curso2);
cursos.add(curso3);
cursos.add(curso4);
```

Rta.

```java
Collections.sort(cursos,Comparator.comparing(Curso::getTiempo).reversed());
```

---

## **Relacionando listas de objetos**

### **Tipos de List**

¿Cuál no es un tipo de List?

Rta.

HashSet

### **Creando listas**

¿Cómo crear una lista de ArrayList?

Rta.

```java
List lista = new ArrayList<>();
```

### **Diferencias entre un ArrayList y LinkedList**

¿Cuál es una de las principales diferencias entre un ArrayList y LinkedList?

Rta.

El método _get_ es más rápido usando ArrayList que LinkedList. ¡Alternativa correcta! Al usar ArrayList, al guardar la posición termina siendo más rápido que un LinkedList.

---

## **Conociendo mas de listas**

### **Métodos de la clase Collections**

¿Cuál método es utilizado para ordenar una lista?

Rta.

```java
sort(..)
```

### **Métodos de la interfaz Stream**

De los siguientes métodos ¿Cuáles pertenecen a la interfaz Stream?

Rta.

filter, findAny, findFirst, mapToInt, map, max, min, anyMatch

---

## **Usando la interfaz Set**

### **Implementaciones de la interfaz Set**

¿Cuál clase no pertenece a la interfaz Set?

```txt
LinkedHashSet
TreeSet
HashSet
ArrayList
```

Rta.

ArrayList. Es una implementación de la interfaz List y no de Set.

### **Ventaja de usar un Set**

¿Cuál es una afirmación correcta al hablar de la interfaz Set?

Rta.

No guarda objetos duplicados. Una de las principales ventajas es cuando adicionamos registros a un Set el no guarda registros duplicados.

---

## **Metodos equals y hashCode**

### **Métodos equals y hashCode**

¿Por qué sobrescribir el método equals y hashCode, en una clase?

Rta.

Porque al usar el método contains usa el hashCode y el equals es usado para comparar valores, es una necesidad primordial sobrescribir estos métodos, para buscar los resultados esperados en nuestras comparaciones

¡Alternativa correcta! Aparte de los descrito en la respuesta, es bueno hacer esa sobrescritura de métodos, para luego tener la capacidad de migrar para alguna otra interfaz sin la mayor preocupación de obtener resultados no esperados.

### **Métodos de la interfaz Set**

¿Qué método podemos usar para buscar valores en un HashSet?

Rta.

```java
contains(....)
```

---

## **Otros tipos de Sets y Iterators**

### **Conociendo de Iterator**

¿Cuál método es usado para recorrer los valores de un Iterator?

Rta.

```java
next()
```

### **Conociendo de Iterator**

¿Cuál método puede usar una lista para trabajar con Iterator?

Rta.

```java
iterator()
```

---

## **Maps**

### **Conociendo de Mapas**

¿Cuál es la mejor definición para mapas?

Rta.

Un mapa es una interfaz, hace parte del package _java.util_ no posee métodos de la interfaz _Collection_, trabaja bajo un concepto de _(llave, valor)_.

### **Conociendo de Mapas**

¿Por qué usar un mapa?

Rta.

Obtener valores a través de una llave única, hace más rápido la búsqueda de información. Un mapa trabaja bajo un concepto de (llave, valor), al tener una llave única por ejemplo un DNI, termina siendo más rápido la búsqueda de información.
