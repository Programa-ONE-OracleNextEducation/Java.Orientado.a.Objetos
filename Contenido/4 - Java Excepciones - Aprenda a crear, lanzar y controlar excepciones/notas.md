# **Java Excepciones - Aprenda a crear, lanzar y controlar excepciones**

## **Pila de ejecucion**

### **Salida del Stack**

Tenemos la siguiente clase con el método main:

```java
public class Principal {

    public static void main(String[] args) {
        System.out.println("MAIN BEGIN");
        m1();
        System.out.println("MAIN END");
    }

    public static void m1() {
        System.out.println("B BEGIN");
        m2();
        System.out.println("B END");
    }

    public static void m2() {
        System.out.println("A BEGIN");
        System.out.println("A END");
    }

}
```

Rta.

```txt
MAIN BEGIN
B BEGIN
A BEGIN
A END
B END
MAIN END
```

### **Sobre Stack**

¿Por qué la JVM usa un Stack?

Rta.

- Para saber cuál método está siendo ejecutado.
- Para organizar la ejecución de los métodos.

Una pila de Java es parte de la JVM y almacena los métodos que se están ejecutando. Además del bloque de código, la pila almacena las variables y referencias de ese bloque. Entonces, la JVM organiza la ejecución y sabe exactamente qué método se está ejecutando, que es siempre el método en la parte superior de la pila. La JVM también sabe qué otros aún deben ejecutarse, que son precisamente los métodos que continúan.

---

## **Tratamiento de excepciones**

### **Acerca de las excepciones**

Marque las alternativas correctas sobre excepciones en Java:

Rta.

- Toda excepción en Java tiene un nombre que la idenfica. Este enfoque hace que sea más fácil de entender que usar números mágicos (códigos de error) como 15, 7012 o 16.
- Las excepciones no controladas caen en la pila de ejecución en busca de alguien que pueda manejarlas.

### **Multi-catch**

Tenemos el siguiente código:

```java
try {
    System.out.println(1/0);
} catch(ArithmeticException ex) {
    ex.printStackTrace();
} catch(NullPointerException ex) {
    ex.printStackTrace();
}
```

Marque la opción que demuestra correctamente el código anterior a través de Multi- Catch:

Rta.

```java
try {
    System.out.println(1 / 0);
} catch (ArithmeticException | NullPointerException ex) {
    ex.printStackTrace();
}
```

A través de un | (pipe) en la cláusula catch, podemos capturar más de una excepción. Con eso, podemos tener un solo bloque para manejar una o más excepciones.

---

## **Lanzando excepciones**

### **Resumiendo, el contenido**

Con base en lo que se ha visto hasta ahora, juzgue las siguientes afirmaciones seleccionando las verdaderas:

Rta.

- Si no se maneja, la excepción cambia el flujo de ejecución del programa. Si no se trata, la excepción cambia el flujo de ejecución del programa y finalizándolo abruptamente.
- Para manejar la excepción, use el bloque try y catch. Con el bloque try y catch, manejamos una excepción que puede ocurrir mientras nuestro programa se está ejecutando, tratándolo de antemano con un código específico.

### **¿Por qué no se lanzó la excepción?**

José está viendo el entrenamiento de Java parte 4: Entendiendo excepciones y, siguiendo al instructor, decidió crear la clase Flujo, para ver cómo funciona el lanzamiento de excepciones.

La clase tiene el método main y otros dos métodos, siendo en el segundo, José instancia una ArithmeticException:

```java
public class Flujo {

    public static void main(String[] args) { System.out.println("Inicio del main"); metodo1();
        System.out.println("Fin del main");
    }

    private    static void metodo1() {
        System.out.println("Inicio del metodo1");
        metodo2();
        System.out.println("Fin del metodo1");

    }

    private static void metodo2() {
        System.out.println("Inicio del metodo2");
        ArithmeticException exception = new ArithmeticException();
    }

}
```

Sin embargo, cuando José ejecutó la clase, se sorprendió, ya que no se lanzó la excepción. Ayuda a José seleccionando la alternativa que resuelve el problema.

Rta.

José instancia la excepción, pero no lo lanzo a través del throw:

```java
private static void metodo2() {
    System.out.println("Inicio del metodo2");
    ArithmeticException exception = new ArithmeticException();
    throw exception;
}
```

No es suficiente crear una instancia de la excepción, es necesario lanzarlo a través del throw.

### **Sobre el lanzamiento de excepciones**

Sobre el lanzamiento de excepciones. Juzgue las siguientes afirmaciones a continuación.

1. Para lanzar una excepción, además de instanciarla, es necesario lanzarla a través del throw.
2. La excepción solo se puede lanzar a través de su referencia, por ejemplo:

```java
ArithmeticException exception = new ArithmeticException();
throw exception;
```

3. Cuando se lanza la excepción, el código deja de ejecutarse abruptamente.
4. Podemos asignar un mensaje a la excepción.

Rta.

La declaración 2 es falsa. La declaración 2 es realmente falsa, ya que no es necesario guardar la excepción en una referencia, pudiendo lanzarla directamente en una sola línea, por ejemplo:

```java
throw new ArithmeticException();
```

---

## **Checked y unchecked**

### **¡Miguel pide ayuda! #1**

Miguel está siguiendo el curso y decidió crear su propia excepción para representar la violación de una regla en su aplicación de stock. Escribió el siguiente código:

```java
public class StockInsuficienteException {
}
```

Luego, todo feliz y genial, fue a usar la excepción recién creada en su aplicación:

```java
public void RealizaCompra(Producto producto) {
    //otras instrucciones aquí..
    if (totalDeProductosEnStock < 0) {
        throw new StockInsuficienteException();
    }
}
```

Pero recibe un error de compilación al crear una instancia de la clase.

Ayude a Miguel indicándole lo que debe hacer para que la aplicación vuelva a funcionar.

Rta.

Miguel necesita hacer con la clase StockInsuficienteException heredar de alguna clase en la jerarquía Throwable. Por ejemplo, RuntimeException:

```java
public class StockInsuficienteException extends RuntimeException {

}
```

### **¡Miguel pide ayuda! #2**

Miguel estaba realmente emocionado con la clase y decidió mejorar su clase de excepción StockInsuficienteException, lo que obligó al compilador a verificar si se estaba manejando la excepción. Para esto, la herencia de la clase cambió de acuerdo con el siguiente código:

```java
public class StockInsuficienteException extends Exception {

    public StockInsuficienteException (String msg) {
        super(msg);
    }

}
```

Sin embargo, el código del método RealizaCompra empezó a dar un error. Analiza como esta:

```java
public void RealizaCompra(Producto producto) {
    //otras instrucciones aquí..
    if (totalDeProductosEnStock < 0) {
        throw new StockInsuficienteException("Stock insuficiente");
    }
}
```

¿Qué debe hacer Miguel para que su método vuelva a compilarse?

Rta.

```java
public void RealizaCompra(Producto producto) throws StockInsuficienteException {
    //otras instrucciones aquí..
    if (totalDeProdutosEnStock < 0) {
        throw new StockInsuficienteException("Stock insuficiente");
    }
}
```

Necesitamos cambiar la firma del método para que quede explícito que puede lanzar tal excepción.

![Checked y unchecked](img/checked_y_unchecked.png "Checked y unchecked")

### **¿Miguel entendió la clase?**

Miguel estudió esta clase cuidadosamente y tomó las notas que se encuentran a continuación. ¿Puedes tu, un erudito, indicar qué declaraciones escribió Miguel incorrectamente a través de las alternativas a continuación?

1. Existe una gran jerarquía de clases que representan excepciones. Por ejemplo, ArithmeticException es hija de RuntimeException, que hereda de Exception, que a su vez es hija de la clase de excepciones más ancestral, Throwable. Conocer bien esta jerarquía significa saber cómo usar las excepciones en su aplicación.

2. Throwable es la clase que necesita ser extendida para poder lanzar un objeto en la pila (usando la palabra reservada throw)

3. La jerarquía que comenzó con la clase Throwable se divide en excepciones y errores. Las excepciones se utilizan en códigos de aplicación. Los errores son utilizados exclusivamente por la máquina virtual. Las clases que heredan de ExceptionError se utilizan para informar errores en la máquina virtual. Los desarrolladores de aplicaciones no deben crear errores que hereden de Error

4. StackOverflowError es un error de la máquina virtual para informar que la pila de ejecución no tiene más memoria.

5. Las excepciones se dividen en dos grandes categorías: las que el compilador comprueba obligatoriamente y las que no. Los primeros se denominan checked y se crean al pertenecer a una jerarquía que no pasa por RuntimeException. Los segundos son unchecked y se crean como descendientes de RuntimeException.

Rta.

Miguel se equivocó en el enunciado 3. La primera oración del enunciado 3 es correcta. Sin embargo, las clases que informan de errores de máquinas virtuales heredan de Error. ¡Dile a Miguel que vuelva a ver los videos!

---

## **Aplicando excepciones**

### **¿Cómo compilar?**

Pedro está estudiando para la certificación OCP-JP de Oracle y recibió el siguiente código:

```java
class ConexionException extends Exception {

    // ------

}

// ------

class Conexion {

    public void cerrar() throws ConexionException {

        //implementación omitida
    }

}

// ------

class TestConexion {

    public static void main(String[] args) {

        Conexion con = new Conexion();

        con.cerrar ();
    }

}
```

La descripción dice que el código no se compila.

¿Puedes ayudar y marcar las opciones que se utilizan para permitir que el código compile?

Rta.

- Simplemente agregue throws ConexionException en la firma del método main. Cuando se comprueba que la excepción es checked, debemos realizar una "acción" en el método main. Una forma es poner throws en la firma del main:

```java
public static void main(String[] args) throws ConexionException {}
```

- Simplemente agregue un bloque try-catch a la llamada con.cerrar() dentro del método main. Como la excepción es checked, debemos realizar una "acción" en el método main. Una forma es usar try-catch:

```java
public static void main(String[] args) {

    Conexion con = new Conexion(); try {
        con.cerrar();
    } catch(ConexionException ex) {
        ex.printStackTrace();
    }

}
```

- En la clase ConexionException, basta extender la clase RuntimeExeption en lugar de Exception. De esta manera, se torna un unchecked y el compilador no obliga más a "tomar una acción”.

### **¿Qué captura?**

Ya hemos visto en este curso dos formas de capturar varias excepciones a través del bloque catch. (1) La forma tradicional, que ha funcionado desde el comienzo de Java, simplemente repite el bloque catch para cada excepción:

```java
try {
    metodoPeligrosoQuePuedeLanzarVariasExcepciones();
} catch(MiExcepcion ex) {
    ex.printStackTrace();
} catch(OtraExcepcion ex) {
    ex.printStackTrace();
}
```

Y (2) la forma más actual, que se introdujo en Java 7, le permite definir las diversas excepciones en el mismo bloque (multi catch):

```java
try {
    metodoPeligrosoQuePuedeLanzarVariasExcepciones ();
} catch(MiExcepcion | OtraExcepcion ex) {
    // multi-catch
    ex.printStackTrace();
}
```

Encontrarás ambas formas en tu día a día como desarrollador Java. Ahora, vea la firma del "método peligroso" en cuestión:

//funciona, podemos colocar dos excepciones en la firma

```java
public void metodoPeligrosoQuePuedeLanzarVariasExcepciones() throws MiExcepcion, OtraExcepcion {
    // código omitido
}
```

Vimos otra variación más de catch, no sintáctica, sino conceptual. ¿Qué declaración a continuación se puede utilizar para capturar todas las excepciones de este "método peligroso"?

Importante: ¡asumiendo que ambas excepciones son de tipo checked!

Rta.

```java
try {
    metodoPeligrosoQuePuedeLanzarVariasExcepciones();
} catch(Exception ex) {
    ex.printStackTrace();
}
```

Creamos un catch genérica que captura cualquier excepción, incluidas las excepciones checked.

Esto puede parecer una buena práctica, pero generalmente no lo es. Como regla general, siempre trate de ser lo más específico posible en el bloque catch favoreciendo multiples bloques de catch o utilizando multi-catch.

### **¿Son útiles las excepciones?**

¿Cómo pueden las excepciones ayudar a mejorar el código de su programa?

1. Las excepciones tienen un nombre y, si ese nombre es expresivo, documenta el problema que está ocurriendo.
2. Las excepciones pueden tener un mensaje, es decir, el problema y el estado de las variables se pueden describir en el mensaje.
3. Las excepciones cambian el flujo de ejecución, es decir, impiden que el problema siga el flujo "normal" cuando ocurre algo excepcional.
4. Se pueden manejar excepciones, es decir, podemos volver a la ejecución "normal" si se resuelve el "problema".

¿Qué opciones son las correctas?

Rta.

Todas las opciones representan una forma de mejorar el código, especialmente la expresividad.

### **¿Como resolver?**

Johann creó una excepción SaldoInsuficienteException, como se muestra en el último video:

```java
public class SaldoInsuficienteException extends Exception {

    public SaldoInsuficienteException(String msg) {
        super(msg);
    }
}
```

Luego cambió la implementación del método sacar para lanzar una excepción:

```java
public abstract class Cuenta {

    //atributos y otros métodos omitidos

    public void sacar(double valor) throws SaldoInsuficienteException {
        if(this.saldo < valor) {
            throw new SaldoInsuficienteException("Saldo: " + this.saldo + ", Valor: " + valor);
        }
        this.saldo -= valor;
    }
}
```

Hasta ahora todo está bien, pero la implementación del método sacar se basa en la clase CuentaCorriente que no quiere compilar:

```java
public class CuentaCorriente extends Cuenta implements Tributable {
    //atributos y otros métodos omitidos

    @Override
    public void sacar(double valor) {
        double valorASacar = valor + 0.2;
        super.sacar(valorASacar);
    }

}
```

¿Cuál es el problema?

Rta.

Como la excepción es un checked, debemos usar throws \*SaldoInsuficienteException en la firma del método sacar de la clase CuentaCorriente. Observe que llamamos a super.sacar(..) en el método sacar de la clase CuentaCorriente. El compilador se da cuenta de que el método de la clase madre tiene el throws de una excepción checked y obliga al método hijo una acción.

### **Para saber más: Nomenclatura**

En el video, usamos una excepción llamada SaldoInsuficienteException. Discutir los nombres puede ser subjetivo y requiere conocimiento del tema. En otras palabras, es objeto de largas discusiones, pero creemos que un nombre un poco más genérico para nuestra excepción también sería una solución adecuada.

Por ejemplo, la excepción podría llamarse SacarException o CuentaException. Tenga en cuenta que usamos el método o el nombre de la clase. Para detallar más el problema (monto del saldo, etc.) podemos usar el mensaje de excepción, como ya hicimos en el curso:

```java
throw new SacarException("Valor invalido: Saldo: " + this.saldo + ", Valor: " + valor);
```

De esa forma, si tiene otro problema, simplemente basta cambiar el mensaje.

De todos modos, sepa que encontrar el nombre perfecto para sus clases y métodos no es una tarea fácil y puede tomar su tiempo. En algunos casos, ya hemos encontrado nombres en las clases que dejaron claro que esto es solo una cosa temporal y que debe cambiarse cuando haya consenso en el nombre.

---

## **Finally y try with resources**

### **Palabras clave**

¿Cuáles son las palabras claves relacionadas con las excepciones?

1. throw
2. finally
3. catch
4. throws
5. try

Rta.

Todas

### **Bloque Finally**

Vea las declaraciones sobre el bloque finally:

1. El bloque finally es opcional cuando hay bloque catch.
2. El bloque finally siempre será ejecutado (sin o con excepción).
3. El bloque finally solo será ejecutado sin excepción.
4. El bloque finally es normalmente utilizado para cerrar un recurso como conexión o transacción.

¿Cuáles son las correctas?

Rta.

1, 2 y 4, porque el bloque finally es opcional, siempre se ejecuta (sin o con excepción) y normalmente se usa para cerrar un recurso.

### **Tratamiento de excepciones**

El equipo de desarrolladores decidió crear dos excepciones para trabajar con cuentas:

```java
public class SacarException extends RuntimeException { }

y

public class DepositarException extends RuntimeException { }
```

Conociendo las dos excepciones, es necesario realizar un tratamiento. ¿Cuáles de las siguientes opciones son válidas (que compilan)?

Rta.

```java
try {

} catch(SacarException | DepositarException ex) {

}

//Correcto, un tratamiento con try y multi-catch.

try {

} finally {

}

//Correcto, en el tratamiento el bloque de catch es opcional cuando existe el bloque finally.

try {

} catch(SacaException ex) {

} catch(DepositarException ex) {

} finally {

}

//Correcto, un tratamiento con try con dos bloques de catch (clásicos) y el bloque Finally.
```

### **Try con recurso**

¿Qué se garantiza cuando usamos try-with-resources?

```java
try(Conexion con = new Conexion()) {
    con.leerDatos();
}
```

Rta.

- El recurso necesita implementar el método close(). Se crea automáticamente un bloque finally. En él se llama al método close() del recurso.
- El bloque finally se crea automáticamente. Se crea automáticamente un bloque finally. En él se llama al método close() del recurso.

### **Para saber más: Excepciones padrones**

En el video, usamos la excepción IllegalStateException, que es parte de la biblioteca de Java e indica que un objeto tiene un estado invalido. Es posible que haya visto otras excepciones famosas, como NullPointerException. Ambos son parte de las excepciones padrones del mundo Java que el desarrollador debe conocer.

Hay otra excepción padrón importante que podríamos haber utilizado en nuestro proyecto. Para ponerlo en contexto, ¿tiene sentido crear una cuenta con una agencia que tiene un valor negativo? Por ejemplo:

```java
Cuenta c = new CuentaCorriente (-111, 222);  //¿tiene sentido?
```

No tiene ningún sentido. Entonces, podríamos verificar los valores en el constructor de la clase. Si el valor es incorrecto, lanzamos una excepción. ¿Cual? La IllegalArgumentException, que es parte de las excepciones de la biblioteca de Java:

```java
public abstract class Cuenta {

    //atributos omitidos

    public Cuenta(int agencia, int numero){

        if(agencia < 1) {
            throw new IllegalArgumentException("Agencia inválida");
        }

        if(numero < 1) {
            throw new IllegalArgumentException("Número de cuenta inválido");
        }

        //resto del constructor fue omitido
    }
}
```

IllegalArgumentException e IllegalStateException son dos excepciones importantes que el desarrollador de Java debería utilizar. En general, cuando tenga sentido, utilice una excepción estándar en lugar de crear la suya propia.
