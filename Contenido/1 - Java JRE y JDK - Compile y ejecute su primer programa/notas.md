# **Java JRE y JDK - Compile y ejecute su primer programa**

## **¿ Que es Java ?**

### **Plataforma java**

La principal ventaja es la plataforma: Maquina virtual, frameworks, librerias open sources, etc.

**Principales caracteristicas**

- Portable
- Facil
- Segura
- Omnipresente

**JVM : problema y solucion**

![jvm_problema](img/jvm_problema.png "JVM Problema")

![jvm_Intermedia](img/jvm_intermedia.png "JVM Intermedia")

![jvm_solucion](img/jvm_solucion.png "JVM Solucion")

### **Beneficios de la JVM**

¿Cuál es el mayor beneficio de la máquina virtual de Java (JVM)?

Rta.

Ejecutar el código independientemente del sistema operativo

En el mundo Java siempre tendremos el mismo archivo "ejecutable" (Bytecode) que será ejecutado por la máquina virtual de Java (JVM) independientemente del sistema operativo estemos usando. De esta forma, no es necesario reescribir el código o adaptarlo a cada sistema operativo. ¡Tenemos un único ejecutable para todas las plataformas!

### **¿Qué sistemas podemos desarrollar?**

Dependiendo del lenguaje de programación, existen varios tipos de sistemas que puedes desarrollar, como por ejemplo:

- Sistemas web (Web sites, aplicaciones web)
- Sistemas solo server side (backend)
- Aplicaciones Android
- Applets para navegadores web

¿Qué tipos de sistemas se pueden desarrollar utilizando Java?

Rta.

Todas

Ya sean aplicaciones web o completamente backend, Android o un viejo applet, Java abarca todos esos campos. Sin embargo, son las aplicaciones web y Android las que tuvieron mayor éxito en el mundo y con más oferta de trabajo para desarrolladores.

### **¿Bytecode vs EXE?**

¿Cuál es la diferencia entre archivos ejecutables de Windows (.exe) y archivos ejecutables de Java (Bytecode)?:

Rta.

- Los ejecutables de Windows pueden correr directamente en el sistema operativo, los de Java necesitan de la máquina virtual

Los programas escritos en Java necesitan de la máquina virtual para ser ejecutados.

- Los archivos ejecutables de Java son portátiles, los de Windows no

Recuerda que la palabra "portátil" se refiere a que el mismo archivo puede ser ejecutado en varios sistemas operativos (Windows, Linux, Mac, etc.).

### **Para saber más: El nombre Bytecode**

Ya hemos hablado que Bytecode es un código de máquina similar a Assembly. Tal vez encontraste extraño el nombre Bytecode (yo también), sin embargo, tiene una explicación muy simple: existe un conjunto de comandos que la máquina virtual de Java entiende, esos comandos son también llamados opcodes (operation code) y cada opcode posee exactamente 1 Byte de tamaño. Entonces, tenemos un opcode de 1 Byte, o más simple, Bytecode.

---

## **El primer programa**

### **Instalación de java**

- [JVM Descarga](https://www.java.com/es/download/ "JVM Descarga")

- [JDK Descarga : para desarrollar en java](https://www.oracle.com/ar/java/technologies/downloads/ "JDK Descarga")

### **JRE y JDK**

![JDK](img/jdk.png "JDK")

### **JRE vs JDK**

¿Cuál de las siguientes afirmaciones son correctas acerca de JRE y JDK?

- El JDK es el ambiente para ejecutar aplicaciones Java y además posee varias herramientas de desarrollo.

El JDK son las herramientas de desarrollo (como el compilador) y también tiene el JRE incluido.

- El JRE es el ambiente para ejecutar una aplicación Java

Si solo queremos ejecutar una aplicación Java, el JRE (Java Runtime Environment) es suficiente.

### **Para saber más: JVM vs JRE vs JDK**

El mundo Java está lleno de siglas de 3 o 4 letras comenzando con la letra J. Ya debes conocer las dos más famosas, JRE y JDK. El primero es el ambiente de ejecución, el segundo son las herramientas de desarrollo junto con el ambiente de ejecución. Simplificando, podemos decir que:

```txt
JDK = JRE + Herramientas de desarrollo
```

Existe una tercera sigla, JVM (Java Virtual Machine), que también hemos usado durante el curso. La responsabilidad de la Java Virtual Machine es ejecutar el Bytecode, entonces ¿Cuál es la diferencia entre el JRE y la JVM si ambos ejecutan Bytecode?

La respuesta es simple: El JRE (Nuestro ambiente de ejecución) contiene la JVM, pero también posee varias librerías incluidas. Es decir, para ejecutar una aplicación Java no solo es necesario tener la JVM, también necesitamos de otras librerías.

Entonces, podemos simplificar:

```txt
JRE = JVM + Librerías
```

Es importante resaltar que no se puede descargar la JVM individualmente. Siempre tendremos que descargar el JRE que tiene la JVM y el conjunto de librerías.

### **Sobre compilación y ejecución**

Lea las siguientes afirmaciones sobre compilación y ejecución de código Java:

1. Durante la compilación ocurre una verificación sintáctica del código fuente.
2. En la compilación y ejecución pueden aparecer errores.
3. La JVM ejecuta Bytecode.
4. El compilador genera Bytecode en caso el código fuente no presente ningún error sintáctico.

¿Cuál o cuáles de esas afirmaciones son verdaderas?

Rta. Todas son verdaderas

### **Compilar y ejecutar**

Pedro esta trabajando por primera vez con el sistema operativo Linux, pero está sorprendido, pues el computador no tiene interfaz gráfica, (solo funciona con línea de comandos).

Ahora, él necesita compilar y ejecutar código Java en esa línea de comandos, pero olvidó los comandos.

Archivo _Programa.java_:

```java
class Programa {

    public static void main(String[] args) {
        System.out.println("Funcionooo!!!");
    }

}
```

¿Qué comandos debe usar para compilar y ejecutar ese código Java?

Rta.

```sh
javac Programa.java
java Programa
```

Nota que pasamos la extensión del archivo (.java) para el comando javac:

```sh
javac Programa.java
```

Y para llamar a la JVM usamos solo el nombre de la clase (sin extensión):

```sh
java Programa
```

### **Haz lo que hicimos: Instalando el JDK**

¡Vamos a instalar el JDK! Para eso, sigue los pasos de abajo de acuerdo a tu sistema operativo. Y al finalizar la instalación, continúa con las configuraciones finales.

**Instalando el JDK en Linux**

En Ubuntu, podemos ejecutar el siguiente comando en la consola de línea de comandos:

```sh
sudo apt-get install oracle-java9-installer
```

**Instalando el JDK en Mac**

En Mac puedes bajar la versión más actual del JDK [¡aquí!](https://www.oracle.com/ar/java/technologies/downloads/ "Descarga JDK")

Después de la instalación en tu Mac, prueba tu Java con los siguientes comandos en la consola:

**Configuración del PATH en Windows**

1. Ve al panel de control, busca y selecciona Sistema, después de eso, haz clic en Configuración avanzada del sistema.

2. En la pestaña "Avanzado", haz clic en variables de entorno y en la ventana "Variables del sistema" selecciona " Path " y haz clic en editar.

3. En esta nueva ventana, haz clic en el botón nuevo y en la línea que fue seleccionada (última), coloca la ruta a tu directorio bin dentro de la carpeta jdk, que a su vez está dentro de la carpeta java.

4. Guarda el cambio haciendo clic en Aceptar y cierra todas las ventanas.

5. Después de hacerlo, cierra la consola de línea de comandos y ábrela nuevamente.

Prueba los siguientes comandos:

```sh
java -version
javac -version
```

---

## **Programando con Eclipse**

### **Haz lo que hicimos: Instala el IDE de Eclipse**

Así como vimos en los videos, ahora descargaremos el IDE de [Eclipse](https://www.eclipse.org/ "Eclipse"), para eso, sigue los pasos a continuación:

1. En primer lugar, ve al sitio web de Eclipse y haz clic en el botón Download.

2. Cuando hacemos clic, el sitio nos redirige a otra página, haz clic en el botón Download debajo de Get Eclipse.

3. El sitio nos llevará a la descarga de acuerdo con tu sistema operativo, así que ahora haz clic en Download y espera para finalizar.

4. Desde aquí, procede a la instalación de acuerdo con tu sistema operativo.

**Windows**

Para instalar el IDE de Eclipse en Windows, sigue estos pasos:

4.1) Ejecuta el archivo que acabas de descargar y espera al instalador.

4.2) El instalador de Eclipse nos dará varias opciones, para este curso, debemos elegir la opción Eclipse IDE for Java Developers.

4.3) En esta nueva parte, haz clic en Install y espera. Cuando se complete la instalación, haz clic en el Launch para abrir Eclipse automáticamente.

**Linux**

Para instalar el IDE de Eclipse en Linux, sigue estos pasos:

4.1) Extrae los archivos y ejecuta el archivo "eclipse-inst"

4.2) El instalador de Eclipse nos dará varias opciones, para este curso, debemos elegir la opción Eclipse IDE for Java Developers.

4.3) En esta nueva parte, haz clic en Install y espera. Cuando finalice la instalación, haz clic en el Launch para abrir Eclipse automáticamente.

**Mac**

4.1) Extrae los archivos y ejecuta el instalador (eclipse).

4.2) El instalador de Eclipse nos dará varias opciones, para este curso, debemos elegir la opción Eclipse IDE for Java Developers.

4.3) En esta nueva parte, haz clic en Install y espera. Cuando finalice la instalación, haz clic en el Launch para abrir Eclipse automáticamente.

### **Sobre IDEs y editores**

Podemos programar en Java usando editores de texto e IDEs. En este contexto, tenemos las siguientes declaraciones:

1. Un IDE es un entorno de desarrollo integrado que centraliza en un solo lugar el compilador del lenguaje utilizado, el editor de texto, la documentación, la base de datos y todo lo que gira en torno a la creación de aplicaciones.
2. Para aquellos que recién comienzan, Eclipse IDE for Java EE Developers es el más recomendado.
3. NetBeans e Intellij son otros IDEs famosos en el mundo de Java.

### **Eclipse Workspace**

¿Qué sabemos sobre el workspace (espacio de trabajo)?

Rta.

- Un workspace es la carpeta predeterminada que se usará para almacenar todos los proyectos creados con el IDE de Eclipse

Alternativa correcta, un workspace es como una carpeta para varios proyectos.

- Cada proyecto Eclipse está dentro de un workspace

Alternativa correcta, un proyecto siempre está dentro de un espacio de trabajo.

### **Proyecto Java**

Marca las alternativas **correctas**:

- La salida de nuestro programa ejecutado por Eclipse se realiza a través de la view console
- Ejecutamos nuestro programa en Eclipse a través del menú Run -> Run as -> Java Application
- Dentro de un proyecto Java, creamos una nueva clase a través de la opción de menú File -> New -> Class

### **Acerca de View Navigator**

Verifica la alternativa correcta con respecto al Navegador de vistas:

Rta.

Es similar al Explorador de Windows de Windows o al Buscador de MAC. Nos permite ver el directorio del proyecto con sus archivos completos.

---

## **Tipos y variables**

### **Para saber más: Type Casting**

Como se ve en los videos, cuando intentamos poner un valor entero en una variable de tipo double, Java no muestra un error. Sin embargo, cuando intentamos poner un doble en una variable del tipo entero, tenemos un error de compilación.

Esta propiedad se produce porque Java convierte implícitamente de un tipo más pequeño a tipos "más grandes". De entero a double, por ejemplo.

Lo contrario no es cierto porque hay pérdida de datos cuando se realiza la conversión. Resultando en un " type mismatch" que muestra que esta instrucción es de tipos incompatibles.

Para realizar una conversión donde puede haber pérdida de información, es necesario hacer un type casting. Vea las instrucciones a continuación.

```java
int edad = (int) 30.0;
```

En el caso anterior, es explícito que se realizará la conversión de doublé a entero. Vea cómo funciona el cast implícito y explícito en la tabla a continuación.

![Type Casting 1](img/type_casting_1.png "Type Casting 1")

Para comparar cada tipo primitivo más claramente, la siguiente tabla muestra el tamaño de cada uno.

![Type Casting 2](img/type_casting_2.png "Type Casting 2")

---

## **Practicando condicionales**

### **Operador lógico**

A continuación, se presentan declaraciones sobre operaciones lógicas en el lenguaje Java. ¿Cuáles son ciertas?

- El operador lógico AND está representado por los caracteres && y el OR por ||.
- Los operadores lógicos deben tener una expresión booleana en los lados izquierdo y derecho.

### **Para saber más: el comando switch**

Vimos cómo hacer pruebas con if, pero ¿si necesitamos hacer varias pruebas? Un ejemplo, tenemos una variable de mes, necesitamos probar su número e imprimir su mes correspondiente. Entonces, ¿vamos a hacer 12 ifs?

Para estos casos, existe el switch, de cambio, donde podemos poner todas las opciones o instrucciones que nuestro programa puede tomar. Funciona de la siguiente manera:

```java
switch (variableASerProbada) {
    case opción1:
        comando (s) si se ha elegido la opción 1
        break;
    case option2:
        comando (s) si se ha elegido la opción 2
        break;
    case option3:
        comando (s) si se ha elegido la opción 3
        break;
    default:
        comando (s) si ninguna de las opciones anteriores ha sido elegida
}
```

El código que se ejecutará, que en nuestro caso será la impresión del nombre del mes, será el código donde se cumple la condición:

```java
public class TestMes {

    public static void main (String [] args) {

        int mes = 10;

        switch (mes) {
            case 1:
                System.out.println ("El mes es enero");
                break;
            case 2:
                System.out.println ("El mes es febrero");
                break;
            case 3:
                System.out.println ("El mes es marzo");
                break;
            case 4:
                System.out.println ("El mes es abril");
                break;
            case 5:
                System.out.println ("El mes es mayo");
                break;
            case 6:
                System.out.println ("El mes es junio");
                break;
            case 7:
                System.out.println ("El mes es julio");
                break;
            case 8:
                System.out.println ("El mes es agosto");
                break;
            case 9:
                System.out.println ("El mes es septiembre");
                break;
            case 10:
                System.out.println ("El mes es octubre");
                break;
            case 11:
                System.out.println ("El mes es noviembre");
                break;
            case 12:
                System.out.println ("El mes es diciembre");
                break;
            default:
                System.out.println ("Mes inválido");
                break;
        }
    }
}
```

El break interrumpirá la ejecución del caso que lo contiene, de modo que los demás no se ejecutarán y, si no se aceptan condiciones, se ejecutará el código default. Por ejemplo:

```java
public class TestMes {

    public static void main (String[] args) {

        int mes = 13;

        switch (mes) {
            case 1:
                System.out.println ("El mes es enero");
                break;
            case 2:
                System.out.println ("El mes es febrero");
                break;
            case 3:
                System.out.println ("El mes es marzo");
                break;
            case 4:
                System.out.println ("El mes es abril");
                break;
            case 5:
                System.out.println ("El mes es mayo");
                break;
            case 6:
                System.out.println ("El mes es junio");
                break;
            case 7:
                System.out.println ("El mes es julio");
                break;
            case 8:
                System.out.println ("El mes es agosto");
                break;
            case 9:
                System.out.println ("El mes es septiembre");
                break;
            case 10:
                System.out.println ("El mes es octubre");
                break;
            case 11:
                System.out.println ("El mes es noviembre");
                break;
            case 12:
                System.out.println ("El mes es diciembre");
                break;
            default:
                System.out.println ("Mes inválido");
                break;
        }
    }
}
```

La impresión será Mes inválido. Entonces, el switch es una solución para los ifs encadenados.

---

## **Controlando flujo con loops**

### **Fijacion del bucle while**

¿Qué afirmaciones a continuación son verdaderas con respecto al while?

Rta.

- En la expresión condicional while es posible usar cualquier operador de comparación: < (menor), > (mayor), <= (menor o igual), >= (mayor o igual), == (igual a) y != (diferente de)) y cualquier operador lógico (&& (and), || (or)).
- El while siempre necesitará una expresión condicional que definirá cuándo se debe romper el ciclo.

### **Enfoque en el comando break**

Clarice tiene dudas sobre cómo funciona el break cuando se usa dentro de bucles anidados. Elige la opción que describa exactamente cómo funciona este comando en estas situaciones.

- Para la ejecución del bucle más interno que contiene el comando break y continúa ejecutando los bucles más externos

### **Ejercitar bucles anidados y rotos**

Después de asistir a las clases de anidación de bucles de repetición y del comando de break, Fernando decidió crear un código como forma de entrenamiento que imprima lo siguiente en la pantalla:

```txt
1
12
123
1234
12345
```

Para esto, creó el siguiente código:

```java
public class ProgramaConBreak {

    public static void main(String args[]) {

        for(int fila = 0; fila < 5; fila++) {
            for (int columna = 0; columna < 5; columna++) {
                if ( ??? ) {
                    break;
                }
                System.out.print( ??? );
            }
            System.out.println();
        }

    }

}
```

Sin embargo, tiene dudas sobre cómo llenar los espacios con ???. Selecciona la opción que muestra las formas correctas de completar el código respectivamente. En caso de duda, realiza la prueba y ejecuta el programa.

Rta.

columna > fila y columna + 1
