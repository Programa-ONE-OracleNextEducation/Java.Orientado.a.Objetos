# **Java y java.lang - Programe con la clase Object y String**

## **Organizando clases con paquetes**

### **Acerca de FQN**

¿Qué es FQN?

Rta.

Es el nombre completo de la clase compuesto por el nombre del paquete y el nombre de la clase. El FQN (Full Qualified Name) es el nombre completo de la clase, que consta del nombre del paquete y el nombre de la clase.

FQN = Nombre del paquete. Nombre de clase simple

### **Acerca de Paquetes**

Vea las afirmaciones sobre Packages:

```txt
A. Los Packages son una forma de distribuir código Java entre proyectos.
B. Los Packages son directorios con un significado especial dentro del código fuente de Java
C. La palabra reservada package debe ser la primera declaración en el código fuente de Java.
D. Los Packages se utilizan para organizar y agrupar clases e interfaces.
```

¿Qué afirmaciones son verdaderas?

Rta.

B, C y D. Dado que los packages son directorios que tienen un significado especial dentro del código fuente de Java (b), la palabra reservada package debe ser la primera declaración (c) y los packages sirven para organizar y agrupar clases e interfaces (d). Solo la declaración (a) es incorrecta.

### **La mejor opción de paquete**

Su amigo Pedro trabaja en la empresa Uberdist en el proyecto con el sobrenombre de udnotas. Pedro también mencionó que la empresa tiene un sitio web con la dirección uberdist.com.br.

Pedro tiene dudas sobre la nomenclatura del package. ¿Qué nombre tendría más sentido y se considera una buena práctica?

```txt
br.com.uberdist.udnotas

Correcto, siempre siguiendo la regla:
NOMBRE_DEL_SITE_AL_REVES.NOMBRE_DEL_PROYECTO
```

### **Estructura correcta**

Ahora hemos visto que en un archivo de clase o de interfaz está la declaración package, la declaración import y la definición de la clase.

```txt
A) Solo puede tener una declaración de package por archivo
B) La declaración import es opcional
C) Es posible repetir la declaración import para importaciones de diferentes packages
D) La definición de clase siempre debe ir por último (después de package e import)
```

¿Qué afirmaciones son correctas?

Rta.

Todas

### **¿Dominas los paquetes?**

En el desarrollo de Java, organizamos nuestras clases en paquetes. Sobre este tema es correcto afirmar que:

Rta.

- Cuando un proyecto usa paquetes, podemos usar import de otras clases para su uso. _import_ facilita el uso de la clase porque no necesitamos el Full Qualified Name.
- Por organización y por la convención adoptada, debemos seguir el dominio de la empresa. Es decir, si la empresa tiene el dominio _alura.com.br_, los paquetes deben ser subpaquetes de br.com.alura. Correcto, pero hay excepciones (pocas).
- El modificador default de Java restringe el acceso a nivel de paquete. Por lo tanto, si no se define ningún modificador, ya sea en la clase, método o atributo, solo las clases del mismo paquete pueden acceder a esta información.

---

## **Todos los modificadores de acceso**

### **Acerca de los modificadores**

Con respecto a los modificadores de acceso de Java, es correcto afirmar que:

Rta.

Tenemos cuatro modificadores: private, protected, default y public. Donde el orden del más restrictivo al menos restrictivo es: private, default, protected y public.

### **¿Cuál modificador? #1**

¿Qué modificador es visible en los siguientes lugares?

![Cual modificador 1](img/cual_modificador_1.png "Cual modificador 1")

Rta.

_protected_. ¡Correcto! El modificador protected funciona al igual que <<package private>> pero agrega visibilidad para los hijos (llamado de "public para los hijos").

### **¿Cuál modificador? #2**

![Cual modificador 2](img/cual_modificador_2.png "Cual modificador 2")

Rta.

**package private** (ausencia de modificador)

En ausencia del modificador, también llamado package private o default, el miembro es visible en la clase y en el package, pero es invisible fuera del package (ni siquiera para los hijos).

---

## **Distribución de código**

### **¿Qué comentario?**

¿Qué comentario deberíamos usar para declarar un javadoc en el código fuente?

Rta.

```java
/ **
* javadoc aquí
* /
```

Correcto, importante es usar el carácter / seguido de 2 caracteres \*\*

### **Sobre javadoc**

Lea las declaraciones con atención:

```txt
A) Debe tener instalado el JDK para poder generar la documentación de javadoc.
B) El javadoc es documentación escrita por el desarrollador para desarrolladores.
C) Todos los miembros de la clase son contemplados en el javadoc.
D) Hay etiquetas especiales para marcar el autor o la versión de la clase.
```

Todos son verdaderos excepto:

Rta.

C. Correcto. No es cierto ya que solo los miembros "públicos" son contemplados en el javadoc. Otros miembros (no públicos) se consideran detalles de implementación y no pueden ser utilizados por otras clases.

### **Para saber más: Todas las etiquetas**

Ya vimos en esta clase algunas tag (o anotaciones) de javadoc como @version o @author.

Aquí está la lista completa:

```java
@author (usado en la clase o interfaz)
@version (usado en la clase o interfaz)
@param (usado en el método y constructor)
@return (usado solo en el método)
@exception o @throws (en el método o constructor)
@see
@since
@seriel
@deprecated
```

Es importante que las etiquetas javadoc existan solo para estandarizar algunos datos fundamentales de su código fuente, como el autor y la versión.

### **Otras anotaciones**

En los cursos también ha visto una anotación fuera del javadoc, la anotación @Override. Esta anotación se considera una configuración, en este caso interpretada por el compilador.

Las anotaciones van mucho más allá de los tags de javadoc y son mucho más sofisticadas y poderosas. Ellas ingresaron a la plataforma Java desde la versión 1.5 mientras que el javadoc ha estado presente desde el nacimiento de la plataforma Java. Lo interesante es que las anotaciones se inspiraron en las etiquetas javadoc.

Si aún no está seguro sobre el uso de las anotaciones, tenga la seguridad de que aún verá muchas usadas por las bibliotecas que se utilizan para definir datos y configuraciones. ¡Espere!

### **¿Qué es un JAR?**

Hablamos en el video de que JAR significa nada más que Java ARchive.

¿Qué más podemos decir sobre el archivo JAR? Marque todas las declaraciones correctas:

Rta.

- Es un archivo comprimido ZIP. No es más que un archivo ZIP, pero con la extensión .jar
- Es el formato estándar en el mundo de Java para distribuir código compilado. Cualquier biblioteca o proyecto usará JAR(s) para distribuir el código.

### **Para saber más: Maven**

Java es una plataforma de desarrollo completa que destaca por su gran cantidad de proyectos de código abierto (open source). Para la mayoría de problemas en el día a día del desarrollador ya existen librerías para solucionar. Es decir, si te gustaría conectarte con una base de datos, o trabajar en desarrollo web, en el área de ciencia de datos, creación de servicios o Android, ya existen librerías para esto, muchas veces más de una.

Existe la necesidad de organizar, centralizar y versionar los JARs de esas librerías y administrar las dependencias entre ellos. Para solucionar esto, se crearon herramientas específicas y en el mundo Java se destacó **Maven**. Maven organiza los JARs (código compilado, código fuente y documentación) en un repositorio central que es público y se puede buscar:

[mvnrepository.com](https://mvnrepository.com/ "mvnrepository.com")

Allí puede ver e incluso descargar los archivos JARs, pero lo mejor es que la herramienta Maven puede hacer esto por usted.

Nota: si es un usuario de Linux, Maven es muy similar a los administradores de apt o rpm. En MacOS existe brew con el mismo propósito. En el mundo .Net tenemos nuget y la plataforma node.js usa npm. La gestión de dependencias es un problema cotidiano para el desarrollador, y cada sistema o plataforma tiene su propia solución.

---

## **El paquete java.lang**

### **Acerca de java.lang**

¿Qué es correcto decir sobre el paquete java.lang?

Rta.

- Tiene clases imprescindibles para cualquier programa. Correcto, las clases String y System son solo dos ejemplos de varios otros.
- No necesita importar, se importa automáticamente. Correcto, no es necesario colocar explícitamente import java.lang. \*
- Las clases String y System provienen de este paquete. Correcto, y veremos varias otras clases de este paquete fundamental.

### **Excepciones de java.lang**

Cuando hablamos de excepciones hemos visto varias clases como Exception, RuntimeException, NullPointerException o ArithmeticException.

Todas estas clases provienen del paquete java.lang, por lo que no fue necesario importarlas.

### **La clase String**

Una de las clases con las que nos topamos a menudo es String. ¿Qué afirmación es correcta sobre ella?

Rta.

- Un objeto de tipo String no se puede modificar. Los objetos de la clase String son inmutables. Esto significa que, una vez creado, no se puede cambiar, por lo que cualquier cambio crea un nuevo objeto String.

- Es una clase definida en java.lang y por lo tanto no es necesario importar, porque la clase String es en realidad del paquete java.lang. El FQN es java.lang.String

### **¿Cuál es el resultado?**

Vea el código:

```java
public class TestString {

    public static void main(String[] args) {

        String nombre = "Mario";
        nombre.replace('o', 'a');
        System.out.println(nombre);
    }
}
```

Al compilar y ejecutar, ¿cuál es el resultado?

Rta.

El código compila y se ejecuta. El resultado es: Mario

Correcto, el método replace no cambia el String original, sino que devuelve un nuevo String. Para contemplar el cambio debemos tomar el resultado del método replace:

```java
String nombre = "Mario";
nombre = nombre.replace('o', 'a');
System.out.println(nombre);
```

### **La interfaz CharSequence**

En los videos, es posible que hayas notado que algunos métodos de la clase String reciben una variable de tipo CharSequence. El tipo CharSequence es una interfaz que la propia clase String implementa (¡ya que String es una secuencia de caracteres!):

```java
public class String implements CharSequence { }
```

Cuando usamos la clase String, incluso podríamos declarar la variable con el tipo de interfaz, pero eso es raro de ver:

```java
CharSequence seq = "es una secuencia de caracteres";
```

Lo interesante es que hay otras clases que también implementan la interfaz CharSequence. En otras palabras, hay otras clases que son secuencias además de caracteres además de la clase String. ¿Por qué?

### **La clase StringBuilder**

Vimos que la clase String es especial porque genera objetos inmutables. Esto se considera beneficioso cuando se piensa en el diseño, pero es malo cuando se piensa en el rendimiento (por eso debemos usar comillas dobles en la creación, ya que la JVM quiere solucionar los problemas de rendimiento con optimizaciones).

Ahora hay un problema: imagina que necesitas crear un texto enorme y necesitas concatenar muchos String, por ejemplo:

```java
String texto = "Ayuda";
texto = texto.concat("-");
texto = texto.concat("me ");
texto = texto.concat("subi ");
texto = texto.concat("en el ");
texto = texto.concat("omnibus ");
texto = texto.concat("equivocado ");
System.out.println(texto);
```

En este pequeño ejemplo ya hemos creado varios objetos, solo porque estamos concatenando algunos String. Esto no es bueno pensando en el rendimiento y para resolver esto existe la clase StringBuilder que ayuda a concatenar Strings de manera más eficiente.

Vea el mismo código usando StringBuilder:

```java
StringBuilder builder = new StringBuilder("Ayuda");
builder.append("-");
builder.append("me ");
builder.append("subi ");
builder.append("en el ");
builder.append("omnibus ");
builder.append("equivocado ");
String texto = builder.toString();
System.out.println(texto);
```

StringBuilder es una clase común. Observe que usamos new para crear el objeto. Además, como el objeto es mutable, usamos la misma referencia, sin nuevas asignaciones.

**La interfaz CharSequence**

Ahora lo bueno es que la clase StringBuilder también implementa la interfaz CharSequence:

```java
public class StringBuilder implements CharSequence {
CharSequence cs = new StringBuilder("También es una secuencia de caracteres");
```

Esto significa que algunos métodos de la clase String saben cómo trabajar con StringBuilder, por ejemplo:

```java
String nombre = "ALURA";
CharSequence cs = new StringBuilder("al");

nombre = nombre.replace("AL", cs);

System.out.println(nombre);
```

Viceversa, la clase StringBuilder tiene métodos que reciben el tipo CharSequence. De esa forma podemos trabajar de forma compatible con ambas clases, basándonos en una interfaz común.

---

## **La clase Object**

### **Propósito de toString()**

¿Cuál es el propósito del método toString() de la clase Object?

Rta.

- El método toString() existe para devolver información sobre el estado del objeto. El método toString debería devolver información sobre el estado del objeto. Es útil para depurar en desarrollo.
- El método toString() existe para ser reemplazado. Es una buena práctica sobrescribir el método para darle un significado mayor que el resultado estándar de ese método.

### **Acerca de Object**

La clase **Object** es la clase raíz de Java, es correcto afirmar:

Rta.

- No es necesario que dejar explícito en la declaración de una clase que debe heredar de Object, porque esto es automático. No hay necesidad de heredar de Object explícitamente, el compilador inserta automáticamente la declaración.
- Cualquier objeto puede ser referenciado por el tipo de Object, ya que es el principal. El tipo de objeto es la forma más genérica de hacer referencia a un objeto.

### **Entendiendo el código**

Está revisando un proyecto de su amigo y encontró el siguiente código:

```java
Cuenta c = new Cuenta();
c.agencia.getCodigo();
```

La clase Cuenta está en otro package y se importó correctamente.

¿Qué podemos decir sobre la parte “agencia” en el código anterior? Verifique todas las alternativas correctas:

Rta.

- “agencia” es un atributo de la instancia. Agencia es un atributo de la instancia, porque usamos la referencia c para acceder al atributo. Si fuera estático, la llamada sería:

```java
Cuenta.agencia.getCodigo();
```

- agencia es una referencia, porque estamos usando. para llamar al método getCodigo(). Si agencia fuera una primitiva, no sería posible llamar a un método.

- agencia tiene visibilidad pública, porque en el enunciado dice que estamos importando las clases correctamente. Esto significa que la clase Cuenta está en otro paquete.

### **Una duda mas**

Aún en el mismo proyecto encontraste otro código:

```java
boolean existe1 = Banco.existeBanco("agencia");
boolean existe2 = Banco.existeBanco(394);
```

Tu amigo tiene problemas para entender. ¿Puedes ayudarlo y marcar todas las alternativas correctas?

- existeBanco es un método. Los paréntesis dejan en claro que se trata de un método.
- existeBanco tiene acceso estático, porque usamos la clase Banco para llamar.
- existeBanco es una forma de sobrecarga. Hay dos versiones de este método que varían en parámetros, a la primera se le da un String y a la segunda se le da un valor int.
