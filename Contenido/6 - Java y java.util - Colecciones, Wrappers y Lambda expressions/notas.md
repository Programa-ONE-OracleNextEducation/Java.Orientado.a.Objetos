# **Java y java.util - Colecciones, Wrappers y Lambda expressions**

## **Conociendo arrays**

### **Acerca de arrays**

Seleccione todas las declaraciones sobre arrays correctas:

Rta.

- Un array es siempre zero-based (El primer elemento se encuentra en la posición 0). La primera posición del array siempre se indica con 0.
- Es una estructura de datos.
- Un array siempre es inicializado con los valores padron. Ya que cada posición del array se inicializa con el valor padrón. ¿Cuál es este valor padrón? El tipo de array define. Por ejemplo, en el array int el valor padrón es 0, en el double el valor es 0.0.

### **La sintaxis del array**

Un array realmente tiene una sintaxis extraña porque usa estos corchetes ( [ ] ). Entonces, en caso de duda, relájese y pruebe cada línea dentro de un método main. No hay problema en cometer errores, pues estamos aprendiendo...

Sabiendo esto, ¿cuál es la forma correcta de crear un array de tipo double?

Rta.

```java
double[] precios = new double[5];
```

Definimos el tamaño al momento de crear el array. Además de la sintaxis presentada en la alternativa, existe otra alternativa (menos utilizada):

### **¿Cuántas referencias?**

Se está preparando para la certificación de Java y ha pasado por el siguiente código:

```java
CuentaAhorro[] cuentas = new CuentaAhorro[10];
CuentaAhorro ca1 = new CuentaAhorro(11,22);
CuentaAhorro ca2 = new CuentaAhorro(33,44);
cuentas[0] = ca1;
cuentas[1] = ca1;
cuentas[4] = ca2;
cuentas[5] = ca2;
CuentaAhorro ref1 = cuentas[1];
CuentaAhorro ref2 = cuentas[4];
```

¿Cuántas referencias apuntan a CuentaAhorro con agencia 33 y número 44?

```txt
Correcto. ¿Vamos a contar?.
CuentaAhorro ca2 = new CuentaAhorro(33,44);
cuentas[4] = ca2;
cuentas[5] = ca2;
CuentaAhorro ref2 = cuentas[4];
```

### **Acceso al array**

Continuando con los estudios para la certificación, encontró otro fragmento de código:

```
CuentaAhorro[] cuentas = new CuentaAhorro[10];
CuentaAhorro ca1 = new CuentaAhorro(11,22);
CuentaAhorro ca2 = new CuentaAhorro(33,44);
cuentas[0] = ca1;
cuentas[4] = ca2;
System.out.println(cuentas[1].getNumero());
```

Ejecutando este tramo de código dentro del método main de nuestro proyecto, ¿cuál es el resultado?

Nota: En caso de duda, pruebe el código, ¡no hay problema!

Rta.

NullPointerException, porque estamos accediendo al segundo elemento del array (posición 1) y esta posición sigue siendo null:

### **Forma literal**

Hasta ahora hemos visto la forma "clásica" de crear un objeto array utilizando la palabra clave new, por ejemplo:

```java
int[] numeros = new int[6];
numeros[0] = 1;
numeros[1] = 2;
numeros[2] = 3;
numeros[3] = 4;
numeros[4] = 5;
```

Sin embargo, también existe una forma literal. Literal, en este contexto, significa usar valores directamente, menos burocrático, más directo. Vea la diferencia:

```java
int[] refs = {1,2,3,4,5};
```

Usamos las llaves {} para indicar que es un array y los valores ya están declarados dentro de las llaves.

---

## **Guardando referencias**

### **Array de clientes**

Ver el código a continuación:

```java
Cliente clienteNormal = new Cliente();
clienteNormal.setNombre("Flavio");

Cliente clienteVip = new Cliente();
clienteVip.setNombre("Romulo");

Object[] refs = new Object[5];
refs[0]  = clienteNormal;
refs[1]  = clienteVip;

System.out.println(refs[1].getNombre());
```

Suponiendo que el código está dentro de una clase con el método main, ¿se compila el código? Y si compila, ¿cuál es el resultado?

Rta.

No compile, debido a la línea:

```java
System.out.println(refs[1].getNombre());
```

Correcto. Observe que nuestro array almacena referencias de tipo _Object_. Al acceder a alguna posición en el array, recibimos una referencia de tipo _Object_:

```java
Object ref = refs[1];
```

Con esta referencia en la mano, no podemos llamar al método _getNombre()_. Para que esto funcione, primero debemos hacer un type cast antes.

### **Cast explícito e implícito**

Ya hemos hablado mucho sobre Type Cast que no es más que convertir de un tipo a otro.

Cast implícito y explícito de primitivos

Para ser correctos, ya hemos visto cast sucediendo incluso antes de definirlo. Tenemos dos ejemplos, el primero en el mundo de los primitivos:

```java
int numero = 3;
double valor = numero; //cast implícito
```

Observe que colocamos un valor de la variable numero (tipo int) en la variable valor (tipo double), sin usar un cast explícito. ¿Esto funciona? La respuesta es sí, ya que cualquier entero cabe dentro de un double. Por eso el compilador se queda quieto y no exige un cast explícito, pero nada le impide escribir lo:

```java
int numero = 3;
double valor = (double) numero; //cast explícito
```

Ahora bien, lo contrario no funciona sin cast, ya que un double no cabe en un int:

```java
double valor = 3.56;
int numero = (int) valor; //cast explícito es exigido por el compilador
```

En este caso, el compilador desecha todo el valor fraccionario y almacena solo el valor entero.

### **Cast implícito y explícito de referencias**

En las referencias, se aplica el mismo principio. Si el cast siempre funciona no es necesario hacerlo explícito, por ejemplo:

```java
CuentaCorriente cc1 = new CuentaCorriente(22, 33);
Cuenta cuenta = cc1; //cast implícito
```

Aquí también podría ser explícito, pero nuevamente, el compilador no lo requiere porque cualquier CuentaCorriente es una Cuenta:

```java
CuentaCorriente cc1 = new CuentaCorriente(22, 33);
Cuenta Cuenta = (Cuenta) cc1; //cast explícito mas desnecess
```

### **Cast posible e imposible**

¿Type cast explícito siempre funciona?

La respuesta es no. El cast explícito solo funciona si es posible, pero hay casos en los que el compilador sabe que un cast es imposible y luego ni compila ni con type cast. Por ejemplo:

```java
Cliente cliente = new Cliente();
Cuenta cuenta = (Cuenta) cliente; //imposible, no compila
```

Como el cliente no extiende la clase de Cuenta ni implementa una interfaz de tipo de Cuenta, es imposible que funcione ese cast, ya que una referencia de tipo de Cuenta nunca puede apuntar a un objeto del tipo de Cliente.

La certificación Java tiene muchas de estas preguntas sobre cast posible, imposible, explícita e implícita. Si pretendes obtener esta certificación, vale la pena estudiar este tema con mucha tranquilidad.

### **¿Qué declaración?**

Vea la jerarquía de clases:

Y la declaración del array:

```java
????[] referencias = new ????[5];

referencias[0] = new Designer();
referencias[1] = new Gerente();

Designer designer = (Designer) referencias[0];
Gerente gerente = (Gerente) referencias[1];
```

¿Qué podemos poner en lugar de ???? para que se compile el código?

Rta.

- _Funcionario_, ya que los tipos Designer y gerente son Funcionarios, basta colocar:

```java
Funcionario[] referencias = new Funcionario[5];
```

- _Object_, ya que el tipo logra guardar cualquier tipo de Object de referencias al código de compilación:

```java
Object[] referencias = new Object[5];
```

### **Cuales casts**

Continuando con la misma jerarquía de clases:

Y la declaración del array:

```java
Funcionario[] referencias = new Funcionario[5];

referencias[0] = new Designer();

???? ref = (????) referencias[0];
```

¿Qué podemos poner en lugar de ???? para que se compile el código?

Rta.

- Designer, Correcto, como el Designer es un Funcionario, el cast es posible (compila) y también funcionará con normalidad.
- Gerente, Correcto, como el Gerente es un Funcionario, el cast es posible (compila) (pero cuando se ejecuta daría ClassCastException).
- Object, Correcto, al final todas las referencias en el array son de tipo Object. Usando Object el cast no necesita ser explícito, basta con:

```java
Object ref = referencias[0];
```

### **Acerca de ClassCastException**

Lea atentamente las declaraciones sobre ClassCastException:

```txt
A) Es del paquete java.lang
B) Es una excepción checked
C) Se lanza cuando falla el type cast
```

Todos son verdaderos excepto:

Rta.

B. Correcto. Todo lo contrario, el ClassCastException es un unchecked ya que extiende de RuntimeException.

---

## **ArraysList y generics**

### **Añadir referencia**

¿Qué sucede si agrega una referencia a la lista sin definir la posición?

Rta.

El elemento se agrega al final de la lista.

### **Desventajas del array**

¿Cuáles fueron las desventajas señaladas al usar arrays?

Rta.

- El array tiene un tamaño fijo (no puede crecer dinámicamente). Correcto. Una vez creada, el array siempre tendrá el mismo tamaño de elemento. Esto es muy malo cuando no sabemos exactamente cuántos elementos necesitamos conservar.
- El array no sabe cuántas posiciones están ocupadas (solo tamaño total). Correcto. Ésta es una gran desventaja. No queremos saber cuántos elementos puede tener un array, sino cuántos elementos existen realmente en el array.
- Sintaxis fuera del estándar "OO Java". Correcto. Los arrays tienen su propia sintaxis, lo que dificulta la lectura del código.

### **Acerca de Arraylist #**

Ve las siguientes afirmaciones acerca de Arraylist.

```txt
A) Conserve las referencias.
B) Es del paquete java.util
C) Utiliza un array internamente
D) Al inicializar es necesario definir el tamaño
```

¿Cuáles son las correctas?

Rta.

A, B y C. java.util.ArrayList es realmente un guardador de referencias y usa un array internamente.

### **Acerca de ArrayList #2**

¿Cuántos elementos puede guardar un objeto de tipo java.util.ArrayList?

El límite es la memoria de la JVM.

### **Código heredado**

Ha encontrado un código heredado que aún no usa genéricos:

```java
ArrayList lista = new ArrayList();
Cliente cliente = new Cliente();
lista.add(cliente);
```

¿Cómo se puede mejorar el código y aplicar genéricos?

Rta.

```java
ArrayList<Cliente> lista = new ArrayList<Cliente>();
```

Correcto, parametrizamos ArrayList usando <>. Hay una variación/simplificación que se incluyó en Java 1.7. El siguiente código es equivalente a la alternativa:

```java
ArrayList<Cliente> lista = new ArrayList<>();
```

### **Beneficios Generics (genéricos)**

Los generics entraron en la versión 1.5 en la plataforma Java y se mejoraron ligeramente en el Java 1.7. ¿Cuáles son los principales beneficios?

Rta.

- Evitar cast excesivos. Correcto, una vez parametrizada la lista, ya no necesitamos más el cast, por ejemplo:

```java
Cliente ref = (Cliente) lista.get(0); // innecesario con generics
```

- El código más legible, ya que el tipo de elementos es explícito. Correcto, al crear la lista queda claro cuál es la intención. Por ejemplo, en la declaración siguiente, está bastante claro que la lista contiene Strings:

```java
ArrayList<String> nombres = new ArrayList<String>();
```

- Anticipar problemas del cast en el momento de la compilación. Correcto, el compilador advierte si pasamos por alto el tipo, por ejemplo:

```java
ArrayList<String> lista = new ArrayList<String>();
lista.add("Nico");
Cuenta c = lista.get(0); //no compila
```

### **Otras formas de inicialización**

_Lista con capacidad predefinida_

Decíamos que el _ArrayList_ es un array dinámico, es decir, debajo de la tela se usa un array, pero sin preocuparse por los detalles y limitaciones.

Ahora piense que necesita crear una lista que represente a los 26 estados de Brasil. Le gustaría usar un _ArrayList_ para "escapar" del array, pero sabe que _ArrayList_ crea un array automáticamente, del tamaño que la clase considere conveniente.

¿No hay alguna forma de crear esta lista definiendo el tamaño del array? Por supuesto que lo es y es muy sencillo. El constructor de la clase _ArrayList_ es sobrecargado y tiene un parámetro que recibe la capacidad:

```java
ArrayList lista = new ArrayList(26); //capacidad inicial
```

La lista sigue siendo dinámica, ¡pero el tamaño de la matriz inicial es 26!

_Lista a partir de otra_

Otra forma de inicializar una lista es basada en otra que es muy común en el día a día. Para esto, _ArrayList_ tiene un constructor más que recibe la lista base:

```java
ArrayList lista = new ArrayList(26); //capacidad inicial
lista.add("RJ");
lista.add("SP");
//otros estados
ArrayList nueva = new ArrayList(lista); //creando basada en la primera lista
```

Cuanto más sepamos sobre las clases estándar de Java, más fácil será nuestro código.

---

## **Equals y mas listas**

### **Firma del método**

El método equals, junto con los métodos toString y hashCode, es uno de los métodos fundamentales de la clase Object.

¿Cuál es la firma correcta para este método?

Rta.

```java
public boolean equals(Object ref)
```

Es público, devuelve boolean y recibe un Objeto.

### **Acerca de equals**

¿Qué es cierto sobre el método equals?

- Debemos sobreescribir para definir el criterio de igualdad. En general, existen los métodos equals, toString y hashCode para sobreescribir.
- La implementación estándar compara las referencias.
- Está definido en la clase Object.

### **Acerca de las listas**

El paquete java.util es extremadamente importante en el desarrollo de Java. Al respecto podemos decir que:

- Todas las listas garantizan el orden de inserción. Correcto, las listas garantizan el orden de inserción. Esto significa que al iterar recibimos los elementos en el mismo orden en que fueron insertados.
- List es una interface, ArrayList y LinkedList son implementaciones.
- Todas las listas tienen un índice. Correcto, las listas siempre tienen un índice (podemos acceder al elemento a través de la posición). Además, hay una característica más: la lista acepta elementos duplicados, pero hablaremos de eso un poco más adelante.

### **LinkedList vs ArrayList**

LinkedList y ArrayList son dos implementaciones diferentes de la interfaz List. LinkedList es una lista doblemente "enlazada" y ArrayList representa un array con redimensión de tamaño dinámico.

Cada implementación tiene sus ventajas y desventajas (en caso de duda, elija ArrayList). Relacione las características con las implementaciones:

```txt
A) Acceso fácil y de rendimiento por el índice.
B) Inserción y eliminación con performance en cualquier posición, también al inicio.
C) Los elementos deben copiarse cuando no hay más capacidad.
D) Acceso más lento por el índice, es necesario investigar los elementos.
```

Rta.

```txt
ArrayList: A e C
LinkedList: B e D
```

### **De Array para List**

De ahora en adelante usaremos las listas para escapar de las desventajas del array. Sin embargo, ¿recuerdas nuestro array String[] del método main? Ciertamente, y no podemos cambiar la firma del método main ya que la JVM no acepta esto. Bueno, dado que no podemos cambiar la firma, ¿no hay forma de transformar un array en una lista? Por supuesto que la hay, y para eso, ya existe una clase que ayuda en esta tarea: java.util.Arrays

La clase java.util.Arrays tiene varios métodos estáticos auxiliares para trabajar con arrays. Vea lo simple que es transformar un array en una lista:

```java
public class Test {

  public static void main(String[] args) {
    List<String> argumentos = Arrays.asList(args);
  }
}
```

Veamos otras características de la clase java.util.Arrays

---

## **Vector e interfaz collection**

### **Vector, mi Vector!**

¿Qué aprendimos sobre Vector?

Verifique todas las alternativas correctas:

Rta.

- Vector es threadsafe. Esta es la gran diferencia entre ArrayList y Vector
- Vector también usa un array. Como ArrayList, Vector también usa una matriz por debajo.
- Vector es una lista, implementa la interfaz List.

### **¿Dominas listas?**

Acerca de listas, marque todas las alternativas correctas:

Rta.

- Las listas son colecciones. Correcto, cualquier lista también es una colección (java.util.Collection).
- Las listas garantizan el pedido de inserción. Correcto, en la iteración los elementos aparecen en el orden de inserción.
- Las listas son secuencias(tienen índice). Correcto, todas las listas tienen un índice, es decir, son secuencias.

### **Acerca de las colecciones**

Vea el esquema del código a continuación:

```java
????<String> vector = new Vector<String>();
```

- Collection. Correcto, java.util.Collection es la interfaz principal de todas las colecciones.
- Vector.
- List. Correcto, java.util.List es la interfaz principal de todas las listas.

---

## **Las clases wrappers**

**¿Qué son las clases Wrapper?** Para todos los tipos de datos primitivos, existen unas clases llamadas Wrapper, también conocidas como envoltorio, ya que proveen una serie de mecanismos que nos permiten envolver a un tipo de dato primitivo permitiéndonos con ello el tratarlos como si fueran objetos.

### **Código confuso**

El autoboxing/unboxing puede resultar confuso. ¿Será que usted desmitifica el siguiente código?

```java
public class TestWrapper {

    public static void main(String[] args){
        Integer ref = Integer.valueOf("3");
        ref++;
        System.out.println(ref);
    }
}
```

¿El código se compila y se ejecuta? ¿Cuál sería el resultado?

Rta.

Compila, ejecuta e imprime 4.

Correcto, en realidad incrementa valor entero, aunque sea una referencia. Lo que sucede detrás de escena es un autoboxing/unboxing en la línea que incrementa la variable (ref ++). Puedes imaginar que esta línea será reemplazada por tres nuevas:

```java
int valor = ref.intValue();
valor = valor + 1;
ref = Integer.valueOf(valor);
```

¡El valor se desenvuelve, luego se incrementa y luego se envuelve nuevamente!

### **Parsing**

Estás leyendo un archivo de texto con varios números. El problema es que los datos vienen como un String, por ejemplo:

```java
String diaComoTexto = "29";
int dia = ????;
```

¿Qué puedes poner en lugar de ???? convertir el texto?

Rta.

```java
- Integer.parseInt(diaComoTexto)
```

Correcto y sería la opción más adecuada porque devuelve un primitivo.
Alternativa correta

```java
- Integer.valueOf(diaComoTexto)
```

Correcto, pero no sería la mejor opción porque devuelve una referencia (y luego se realiza un unboxing).

### **¿Qué primitivo?**

Vea el código a continuación.

```java
Character cRef = new Character('A');
```

¿Cuál es el tipo primitivo de este wrapper?

Rta.

_char_ Correcto. Lo interesante es que el char es casi un tipo numérico. Tiene dos bytes, al igual que el tipo short, pero no usa el primer bit para almacenar el signo. En otras palabras, el char solo almacena números positivos. Esto significa que el char puede almacenar valores entre 0 y 65536 (2 ^ 16).

### **¿Conoces los wrappers?**

Quiere ayudar a Pedro, que está trabajando con Java pero nunca aprendió los genéricos. Él te mostró el siguiente código:

```java
List referencias = new ArrayList(); //AQUÍ
referencias.add(Double.valueOf(30.9));
referencias.add(Integer.valueOf(10));
referencias.add(Float.valueOf(13.4f));
```

¿Con qué código puedes reemplazar la línea con el comentario //AQUÍ para usar generics?

Rta.

```java
- List<Object> referencias = new ArrayList<>();
```

Correcto, todas las referencias también son Objetos.

```java
- List<Number> referencias = new ArrayList<>();
```

Correcto, todos los elementos de esa lista son números.

### **El Wrapper Integer**

En el último video vimos el primer Wrapper: java.lang.Integer

En este contexto, ¿qué son los Wrappers?

Rta.

¡Son clases que contienen funcionalidades y encapsulan la variable de tipo primitivo! Correcto, creamos objetos de esas clases para envolver o envolver un valor primitivo. Hay varias funcionalidades en estas clases que ayudan en el día a día que veremos en el siguiente video.

---

## **Ordenando listas**

### **Interfaces para ordenar**

En el mundo de Java, existen dos interfaces para definir los criterios para ordenar los elementos de una lista.

Las interfaces son:

Rta.

- java.lang.Comparable. Correcto, para definir el orden natural de los elementos
  Alternativa correta

- java.util.Comparator. Correcto, el comparator es un parámetro del método sort de la lista y de la clase Collections.

### **Orden natural**

En el contexto de la lección, ¿qué significa orden natural?

Rta.

Es el orden definido por el propio elemento de la lista. Correcto, en nuestro ejemplo, la clase Cuenta define sus propios criterios de ordenación implementando la interfaz java.lang.Comparable. En este caso, no se utiliza ningún Comparator.

### **¿Por qué no funciona?**

Su amigo está aprendiendo Java, pero por una razón el código siguiente no funciona. Primero, analice las dos clases (suponga que las importaciones son correctas):

```java
class Programa {
    public static void main(String[] args) {
        Leccion leccionIntro = new Leccion("Introducción a las Colecciones", 20);
        Leccion leccionModelando = new Leccion("Modelando la clase Leccion", 18);
        Leccion leccionSets = new Leccion("Trabajando con Conjuntos", 16);

        List<Leccion> lecciones = new ArrayList<Leccion>();
        lecciones.add(leccionIntro);
        lecciones.add(leccionModelando);
        lecciones.add(leccionSets);

        Collections.sort(lecciones);

        for (Leccion leccion : lecciones) {
            System.out.println(leccion);
        }
    }
}

class Leccion {

    private String titulo;
    private int tiempo;

    public Leccion(String titulo, int tiempo) {
        this.titulo = titulo;
        this.tiempo = tiempo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getTiempo() {
        return tiempo;
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }

    public String toString() {
        return "[título: " + titulo + ", tiempo: " + tiempo + " minutos]";
    }
}
```

El problema es que el código ni siquiera compila. ¿Usted sabe por qué?

Nota: En caso de duda, siempre pruebe, experimente y compruebe el resultado, no hay problema, ¡ya que estamos aprendiendo!

Rta.

La clase Leccion no implementa la interfaz Comparable y, por lo tanto, no implementa el método compareTo. Entonces el código ni siquiera compila. Correcto. ¡Eso mismo! La interfaz Comparable requiere una implementación del método compareTo, que necesita ser llamado por el algoritmo interno de el método sort() de la lista.

---

## **Clases anonimas y lambdas**

Las **clases anónimas** se utilizan en lugar de clases locales para clases con muy poco código, de las que sólo hace falta un objeto. No pueden tener constructores, pero sí inicializadores static o de objeto. Además de las restricciones citadas, tienen restricciones similares a las clases locales.

### **¿Cual objeto?**

¿Cómo llamamos a este objeto que solo encapsula una función / método / procedimiento?

Rta.

Function Object. Un objeto que creamos para encapsular una función o método. Las clases anónimas facilitan la creación de estos objetos.

### **¿Qué sucede?**

Vea el código a continuación:

```java
Comparator<String> comp = new Comparator<String>() {

  @Override
  public int compare(String s1, String s2) {
    return s1.compareTo(s2);
  }
};
```

Y las declaraciones

```txt
A) Se genera una clase anónima.
B) Se crea un objeto de tipo Comparator.
C) Se crea una instancia de la interfaz Comparator.
D) Se genera una clase con el nombre ComparatorString.
```

¿Qué afirmaciones son correctas?

Rta.

A y B.

### **¿Y si fuera lambda?**

Consulte el código heredado escrito a continuación con una versión de Java anterior a 1.8. Tenga en cuenta que este código todavía usa aún una clase anónima en el método sort:

```java
List<String> nombres = new ArrayList<>();
nombres.add("Super Mario");
nombres.add("Yoshi");
nombres.add("Donkey Kong");

Collections.sort(nombres, new Comparator<String>() {

    @Override
    public int compare(String s1, String s2) {
        return s1.length() - s2.length();
    }
});

System.out.println(nombres);
```

¿Cómo sería la implementación de la llamada al método sort con lambda? Verifique las implementaciones correctas:

Rta.

```java
nombres.sort((s1, s2) ->  {return s1.length() - s2.length();} );
```

Correcto! También puede utilizar el método sort de la lista. También tenga en cuenta que el return es opcional (siempre que también saquemos el {}):

```java
Collections.sort(nombres, (s1, s2) ->  s1.length() - s2.length());
```

Correcto. ¡Fíjate que no es necesaria ningún return! Mucho más sencillo y conciso.

### **Foreach con lambda**

Vea el código usando un for "tradicional" para:

```java
List<String> nombres = new ArrayList<>();
nombres.add("Super Mario");
nombres.add("Yoshi");
nombres.add("Donkey Kong");

for(int i = 0; i < nombres.size(); i++) {
    System.out.println(nombres.get(i));
}
```

Te gustaron las lambdas y te gustaría usarlas en el momento del bucle. ¿Qué alternativa usa correctamente la expresión lambda para iterar los elementos de la lista?

Nota: todas las alternativas a continuación realmente funcionan, pero sólo una usa lambdas.

### **El patron Iterator**

Ahora sabes que hay muchas colecciones. Solo en este entrenamiento vimos ArrayList, LinkedList y Vector. Si aún ve el curso dedicado a las colecciones, aprenderá las interfaces para cola (Queue), conjunto (Set) y mapa (Map) cada una con varias implementaciones.

Aquí viene una pregunta: ¿Cómo puedo acceder (iterar) a todas estas implementaciones de manera uniforme sin conocer los detalles de cada implementación? La respuesta está en el "cuadro de patrón de diseño" y se llama Iterador.

Un Iterador es un objeto que tiene al menos dos métodos: hasNext() y next(). Es decir, puede usarlo para preguntar si hay un próximo elemento y pedir el próximo elemento. La buena noticia es que funciona con TODAS las implementaciones y esa es la gran ventaja.

Vea el código para usar el Iterador de una lista:

```java
List<String> nombres = new ArrayList<>();
nombres.add("Super Mario");
nombres.add("Yoshi");
nombres.add("Donkey Kong");

Iterator<String> it = nombres.iterator();

while(it.hasNext()) {
  System.out.println(it.next());
}
```

Si comprende este código, ya ha aprendido a iterar con colas, conjuntos o mapas. Vea el uso de Iterator a través de un conjunto:

```java
Set<String> nombres = new HashSet<>();
nombres.add("Super Mario");
nombres.add("Yoshi");
nombres.add("Donkey Kong");

Iterator<String> it = nombres.iterator();

while(it.hasNext()) {
  System.out.println(it.next());
}
```
