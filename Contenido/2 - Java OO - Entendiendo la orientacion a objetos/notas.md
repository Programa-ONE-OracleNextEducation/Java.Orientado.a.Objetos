# **Java OO - Entendiendo la orientacion a objetos**

## **El problema del paradigma procidimental**

**¿ Que ventajas tiene conocer orientacion a objetos ?**

- Comprender conceptos basicos de encapsulamiento.
- Creacion de codigo reutilizable.
- Ampliamente utilizado por la mayoria de lenguajes de programacion.
- Tecnicas para consumir codigo creado en otras librerias.

### **Idea central del paradigma OO**

Según lo que escuchó en el video, seleccione la alternativa que exprese la idea central del paradigma de Orientación de objetos.

Rta.

Los datos y la funcionalidad de una entidad van de la mano.

### **Un poco de programación procedural**

De acuerdo con las situaciones mencionadas en el video, elija la única alternativa que no sea un signo de la programación procedural.

Rta.

Múltiples equipos trabajando en un solo proyecto.

---

## **Introduccion a orientacion a objetos**

### **Características de los objetos**

¿Cómo llamamos, en orientación a objeto, las características de una clase?

Rta.

Atributo

### **Definiendo tipos**

- Una clase es una especificación de un tipo, que define atributos y comportamientos.
- Un objeto es una instancia de una clase donde podemos definir valores para sus atributos.
- Para crear una instancia, es obligatorio completar los valores de todos los atributos.
- Para crear una instancia necesitamos usar la palabra clave new

### **Establecer valor de atributo**

Jonas creó un objeto de tipo Persona para representar un personaje de un juego que está creando.

Observe la clase que creó:

```java
public class Persona {

   String nombre;
    int edad;
    int peso;

}
```

¿Cuál de las siguientes es la opción correcta para crear un objeto y establecer un valor para sus atributos?

Rta.

```java
Persona heroe = new Persona();
heroe.nombre =  "Jonny";
```

### **Referencias a objetos**

Usando lo aprendido sobre referencias y asignación de valores definiremos una clase a continuación.

```java
public class Cuenta {
    double saldo;
}
```

A partir de esta clase, diga qué imprime el código:

```java
public class Test {

    public static void main(String[] args) {

        Cuenta miCuenta = new Cuenta();
        miCuenta.saldo = 500.0;
        Cuenta otraCuenta = miCuenta;
        otraCuenta.saldo += 1000.0;

        System.out.println(miCuenta.saldo);
    }
}
```

Rta.

Imprime 1500.0

---

## **Definiendo comportamientos**

### **Métodos**

¿Que aprendimos sobre métodos?

- Es posible que un método no tenga parámetros.
- Un método define el comportamiento o la forma de hacer algo.}
- Por convención, el nombre del método en el mundo Java debe comenzar con una letra minúscula.

### **¿Cómo llamar a un método?**

¿Cuál es la sintaxis y el orden correctos para llamar a un método con Java?

Rta.

nombreDeReferencia.nombreDelMetodo();

### **¿Conoces this?**

Rta.

- El _this_ es una palabra clave.

  El _this_ es una palabra clave igual al _void, class, new, int_ y varias otras. El IDE Eclipce muestra todas las palabras claves en un color diferente, el color "púrpura".

- El _this_ es una referencia.

  El _this_ esta es una referencia, es decir, "apunta" a un objeto.

### **Métodos validos**

Suponiendo que cada método a continuación está dentro de una clase, ¿qué declaraciones son válidas (compilan)?

```java
void deposita(){
}

void deposita(double valor, int numero){
}
```

### **¿Dónde usar this?**

Ana está practicando OO con Java y creó otra clase Cuenta con solo dos atributos y un método. Sin embargo, dado que el uso de this es opcional, ella tiene dudas sobre dónde usar la palabra clave this dentro de una clase.

Ella nos envió el código a continuación usando los caracteres [ ] en varios lugares, aquí está el código:

```java
class Cuenta {

    [1] double saldo;
    int numero;

    void deposita([2] double valor) {
        [3]saldo = [4]saldo + [5]valor;
    }
}
```

Observe que tenemos [1], [2], [3], [4] y [5] como lugares posibles para poner this, pero ¿cuál realmente funcionará y se compilará?

Rta.

Solo [3] y [4]

### **Declaración de método**

Pedro escribió el siguiente método, que no está compilando:

// asumiendo que este método está dentro de la clase Cuenta que tiene los atributos

```java
public void sacar(double valor) {

    if(saldo >= valor) {
        saldo -= valor;
        return true;
    } else {
        return false;
    }
}
```

¿Qué hay de malo con el método?

Rta.

Era necesario definir el tipo correcto de retorno en el método.

El método no puede declararse como void. Es correcto usar el tipo boolean:

```java
public boolean sacar(double valor) {

    if(saldo >= valor) {
        saldo -= valor;
        return true;
    } else {
        return false;
    }
}
```

---

## **Composición de objetos**

### **Extrayendo lo que es común**

Juárez creó las siguientes clases:

```java
public class Persona {

    String nombre;
    String numeroIdentidad;
    int edad;
    String direccion;
    String complemento;
    String numero;
    String vecindario;
    String ciudad;
    String codigoPostal;

}

public class Empresa {

    String razonSocial;
    String numeroIdentidadSocial;
    String direccion;
    String complemento;
    String numero;
    String vecindario;
    String ciudad;
    String codigoPostal;
}
```

Podemos ver que los atributos dirección, complemento, numero, vecindario, ciudad y codigoPostal son los mismos en ambas clases. Esta información es un fuerte candidato para ser externalizado en la clase de Direccion y asociado a las clases Persona y Empresa a través de la composición.

```java
public class Direccion {

    String calle;
    String complemento;
    String numero;
    String vecindario;
    String ciudad;
    String codigoPostal;

}
```

Marque la única alternativa verdadera que modifica correctamente las clases Persona y Empresa para usar la clase Direccion.

Rta.

```java
public class Persona {

    String nombre;
    String numeroIdentidad;
    int edad;
    Direccion direccion;
}

public class Empresa {

    String razonSocial;
    String numeroIdentidadSocial;
    Direccion direccion;
}
```

---

## **Encapsulamiento y visibilidad**

### **Público x Privado**

Luan decidió crear un modelo para entrenar la orientación y encapsulación de objetos:

```java
public class Cliente {
    String nombre;
    private String numeroIdentidad;
    int edad;
}
```

Y está creando un objeto Cliente en la otra clase:

```java
public class Banco {

  public static void main(String[] args) {
    Cliente cliente = new Cliente();
    cliente.nombre = "José";
    cliente.numeroIdentidad = "12312312312";
    cliente.edad = 49;
  }
}
```

Rta.

No compila porque el numeroIdentidad es privado.

### **Creando Getters e Setters**

Paulo está creando getters y setters para la siguiente clase:

```java
public class Alumno {
    private String nombre;
    private String matricula;
}
```

¿Cómo sería, siguiendo la convención explicada en el video anterior, la declaración de getters y setters para los dos atributos de la clase?

```java
public String getNombre(){
   return this.nombre;
}

public String getMatricula(){
   return this.matricula;
}

public void setMatricula(String matricula){
   this.matricula = matricula;
}

public void setNombre(String nombre){
   this.nombre = nombre;
}
```

---

## **Constructores y miembros estáticos**

### **Utilizando constructores**

A continuación, se presentan algunas declaraciones sobre el uso de los constructores, ¿cuál es cierto?

Rta.

Los constructores se utilizan para inicializar los atributos.

### **¿Dónde está el error?**

Todavía en el juego de Luan, tenemos otro fragmento de código:

```java
public class Juego {
    //Código omitido

    private Componente comp;
    public Juego(Usuario usuario){
       this.comp = usuario;
    }
}
```

Sin embargo, el código anterior ni siquiera se compila. ¿Cuál de los siguientes motivos explica el motivo de este evento?

Rta.

Se asignan objetos de diferentes tipos.

### **¿Por qué no suma?**

Luan desarrolló un juego y siempre quiere mantener actualizada la cantidad de jugadores. Para esto, Luan escribió el siguiente código:

```java
public class Jugador {
    //Código omitido
    private int total = 0;

    public Jugador(//atributos){
       total++;
    }
}
```

Sin embargo, el contador siempre muestra 1 después de insertar un nuevo jugador. ¿Cuál de los siguientes motivos explica el motivo de este evento?

Rta.

El _total_ debe ser estático, por lo que cada vez que se crea un nuevo objeto de tipo Jugador, no se crea un nuevo total, manteniendo el valor correcto.
