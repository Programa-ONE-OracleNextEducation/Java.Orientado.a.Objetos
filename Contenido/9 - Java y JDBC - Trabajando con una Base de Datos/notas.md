# **Java y JDBC - Trabajando con una Base de Datos**

## **Introducción a JDBC**

### **¿ Que es JDBC ?**

JDBC te permitirá conectarte con una base de datos vía TCP (Transmission Control Protocol). Para ello, existen drivers de JDBC (Java Database Connectivity) que trabajan con todos los grandes proveedores de bases de datos y con muchos otros lenguajes de programación.

Por otra parte, esta herramienta API (Interfaz de programación de aplicaciones) concede el acceso y marca la ruta de acceso a los datos almacenados a partir de las peticiones de sus clientes. Gracias a esto, podrás brindar agilidad y velocidad en las consultas y sus respuestas.

Por último, esta API te ayudará en este proceso por medio de sus métodos de actualización y procesamiento de los datos. Además, podrás contar con los drivers de JDBC como un pilar en la rapidez en que se reciben y se solucionan las peticiones.

![Intro a JDBC](img/intro_a_jdbc.png "Intro a JDBC")

El paquete de Java provee una interfaz que permite conectarnos a los distintos drivers de los distintos proveedores de base de datos.

**Driver MySQL**

```xml
<dependencies>
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <version>8.0.26</version>
    </dependency>
</dependencies>
```

Los drivers que descarguemos en nuestra app, implementan la interfaz JDBC de java. De esta forma la abstraccion de JDBC vamos a poder conectarnos a cualquier base de datos (MySQL, Server SQL, Oracle, etc) sin tener que cambiar ninguna linea de codigo, independientemente de cual sea el proveedor de BD.

**Interfaz JDBC**

```java
package com.alura.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class PruebaConexion {

    public static void main(String[] args) throws SQLException {
        Connection con = DriverManager.getConnection(
                "jdbc:mysql://localhost/control_de_stock?useTimeZone=true&serverTimeZone=UTC",
                "root",
                "root1234");

        System.out.println("Cerrando la conexión");

        con.close();
    }

}
```

### **API e Interfaces**

¿Cuál es la ventaja de utilizar una API como la JDBC, basada en interfaces, para realizar la comunicación entre el código y una base de datos relacional?

Rta.

Transparencia a la hora de elegir la base de datos o cambiar de una para otra, con muy pocos cambios de código. Lo único que va a ser necesario hacer es cambiar la dependencia del driver de base datos y el JDBC se encarga de todo.

### **Excepción cuando se recupera la conexión**

José ha desarrollado el siguiente código para abrir y cerrar una conexión con la base de datos:

```java
public class PruebaConexionConBaseDeDatos {
    public static void main(String[] args) throws SQLException {
        Connection con = DriverManager
            .getConnection("jdbc:mysql://localhost/control_de_stock?useTimezone=true&serverTimezone=UTC");

        con.close();
    }
}
```

Pero cuando va a ejecutar el código le aparece la siguiente excepción: **Access denied for user**.

¿Qué falta hacer para lograr conectarse con éxito a la base de datos?

Rta.

Se necesita agregar los parámetros de usuario y contraseña en el método de conexión.

```java
DriverManager
            .getConnection("jdbc:mysql://localhost/control_de_stock?useTimezone=true&serverTimezone=UTC", "root", "root1234");
```

### **Lo que aprendimos**

Lo que aprendimos en esta aula:

- Para acceder a una base de datos necesitamos del driver de conexión;
  - Un driver es simplemente una librería .jar.
- JDBC significa Java DataBase Connectivity;
  - El JDBC define una capa de abstracción entre la aplicación y el driver de la base de datos.
  - Esta capa es compuesta de interfaces que el driver implementa.
- Para abrir una conexión con la base de datos debemos utilizar el método getConnection de la clase DriverManager;
  - El método getConnection recibe tres parámetros. Son ellos la URL de conexión JDBC, el usuario y la contraseña.

---

## **Ejecutando comandos SQL en Java**

### **Connection, Statement y ResulSet**

¿Que hay en común entre las clases java.sql.Connection, java.sql.Statement y java.sql.ResultSet?

Rta.

Todas son interfaces. Connection, Statement y ResultSet son algunas de las interfaces del paquete java.sql.

### **El estándar Factory**

¿Cuál es la ventaja de utilizar una ConnectionFactory en nuestra aplicación?

Rta.

Proveer una forma más sencilla de crear un objeto. Los objetos son creados sin exponer la lógica o las configuraciones de creación al cliente. Además, es posible referirnos al objeto recién creado utilizando una interfaz (una abstracción), desacoplando la implementación.

### **Retorno del método execute()**

¿Para qué sirve el retorno del método execute de la interfaz java.sql.Statement?

Rta.

El método devuelve true cuando el resultado devuelve un java.sql.ResultSet (resultado de un SELECT) y false cuando el resultado no devuelve contenido (resultado de un INSERT, UPDATE o DELETE).

### **Lo que aprendimos**

Lo que aprendimos en esta aula:

- Para simplificar y encapsular la creación de la conexión debemos utilizar una clase ConnectionFactory;
  - Esta clase sigue el estándar de creación Factory Method, que encapsula la creación de un objeto.
- Podemos utilizar la interfaz java.sql.Statement para ejecutar un comando SQL en la aplicación;
  - El método execute envía el comando para la base de datos.
  - A depender del comando SQL, podemos recuperar la clave primaria o los registros buscados.

---

## **Realizando mejoras en el proyecto**

### **Sobre el PreparedStatement**

¿Cuál es el riesgo de utilizar un Statement en lugar del PreparedStatement?

Rta.

El Statement no mantiene una versión de la query compilada en la base de datos. El PreparedStatement mantiene la query compilada en la base de datos, de forma parametrizada. Así el usuario puede ejecutar la misma consulta diversas veces con parámetros distintos.
Para dejar nuestra aplicación más segura y legible utilizamos el PreparedStatement. Con eso eliminamos la vulnerabilidad de sufrir ataques de SQL Injection.

### **Migrando para el PreparedStatement**

José estaba actualizando su aplicación para utilizar el PreparedStatement y aprovechar los beneficios que esta interfaz provee. Su código quedó de esta forma:

```java
Statement stm = con.prepareStatement("DELETE FROM CLIENTE WHERE NOMBRE = ? AND DNI = ?");

stm.setString(1, "Jose");
stm.setString(2, "12345678");
```

Pero su código no compila correctamente. El Eclipse dice que hay un error en las líneas que setean valores a los atributos de la query. ¿Qué sería necesario arreglar para que el código compile?

Rta.

José necesita actualizar la interfaz del objeto de Statement para PreparedStatement. La interfaz Statement no conoce el método setString. Este método pertenece a la interfaz PreparedStatement.

### **JDBC y transacciones**

¿Cuál es el estándar de JDBC (del driver) para manejar transacciones de base de datos?

Rta.

Auto-Commit. Este es el estándar, que puede ser modificado por el método setAutoCommit, de la interfaz Connection.

### **Auto-Commit**

José ha decidido que iría manejar las transacciones de la aplicación en lugar de JDBC y realizó el seteo del Auto-Commit para false ¿Que más es necesario para que Jose tenga el control total de las transacciones?

Rta.

Hay que explicitar el commit y el rollback. Si la transacción es exitosa, José necesita realizar el commit explícito. Pero si hay un error en la transacción, José también necesita realizar el rollback explícito.

### **try-with-resources y el método close()**

¿Por qué cuando utilizamos el try-with-resources no hay más la necesidad de explicitar el comando close para cerrar los recursos (PreparedStatement, Connection, PreparedStatement)?

Rta.

Por el hecho de que estos recursos extienden la interfaz AutoCloseable. Como estas interfaces extienden la interfaz AutoCloseable, el try-with-resources ejecuta el comando close implícitamente.

### **Lo que aprendimos**

Lo que aprendimos en esta aula:

- Cuando ejecutamos una query SQL como Statement tenemos un riesgo de seguridad llamado SQL Injection;
  - SQL Injection es el hecho de enviar comandos SQL como parámetro de las solicitudes en una aplicación.
- Para evitar el fallo por SQL Injection debemos utilizar la interfaz PreparedStatement;
  - Diferente del Statement, el PreparedStatement trata los parámetros del comando SQL para que caracteres y comandos especiales sean tratados como strings.
- Las bases de datos ofrecen un recurso llamado transacción, que junta muchas operaciones SQL como un conjunto de ejecución;
  - Si el conjunto falla no es aplicada ninguna modificación y ocurre el rollback de la transacción.
  - Todos los comandos del conjunto necesitan funcionar para que la transacción sea finalizada con un commit.
- Para garantizar el cierre de los recursos abiertos en el código, Java provee un recurso llamado try-with-resources para ayudarnos;
  - Para utilizar este recurso es necesario que la clase utilizada (como la Connection) implemente la interfaz Autocloseable.

---

## **Escalabilidad con pool de conexiones**

### **¿ Que es el Pool de conexiones ?**

Un pool de conexiones a base de datos es una forma de llevar un control de las mismas de modo que al desocupar una conexión, en lugar de cerrar la conexión la tengamos a la mano y se entregue al siguiente objeto que necesite una conexión.

Si bien esto puede sonar innecesario debe de recordar que crear una conexión a base de datos es una de las operaciones mas pesadas que puede efectuar y que las conexiones expiran automáticamente después de un tiempo sin uso, ambas cosas que pueden afectar el rendimiento o estabilidad de su aplicación.

Ahí es donde entra el pool de conexiones ya que este se encargara de reducir el numero de veces que necesite crear una nueva conexión y en caso que esta expire crea una nueva si es necesario.

**Pool de conexiones C3P0**

Uno de los pools de conexiones mas populares actualmente es el C3P0 proporcionado por Machinery For Change, Inc, este le proporciona todas las funcionalidades esperadas a la vez que se integra con gran facilidad a otros frameworks de Java.

Debido a su popularidad y sencillez de uso nos enfocaremos en como hacer uso de este pool en particular.

### **Aplicación con cliente único**

Imagina una aplicación que tiene un único cliente y él puede ejecutar una sola tarea por turno. Mientras una tarea termina no es posible ejecutar la siguiente. ¿Qué harías en esta aplicación para atender este escenario?

Rta.

- Abrir una nueva conexión siempre que haya una nueva tarea para hacer. Con la forma que funciona la aplicación este abordaje es válido ya que solamente necesitaríamos una sola conexión.

- Abrir una sola conexión y mantenerla siempre abierta. Es un abordaje válido ya que la aplicación tiene un único cliente.

### **Aplicación con múltiples clientes**

En un escenario donde múltiples clientes pueden acceder a una misma aplicación simultáneamente. ¿Cuál sería el mejor abordaje?

Rta.

- Reutilizar un conjunto de conexiones de tamaño fijo o dinámico. ¡Alternativa correcta! Esta es la estratégia de mantener un pool de conexiones. Vamos a abrir una cantidad específica de conexiones y reutilizarlas.
- Abrir una nueva conexión por cada nuevo usuario y mantenerla abierta todo el tiempo. ¡Alternativa incorrecta! Este abordaje puede crear problemas graves para la base de datos porque la conexión es un recurso costoso. Una base de datos tiene un límite de conexiones, que depende del servicio, infraestructura y configuraciones, que debe ser respetado.
- Abrir una sola conexión y mantenerla siempre abierta. ¡Alternativa incorrecta! Este abordaje no es escalable porque solamente vamos a atender un único cliente por turno. Es importante recordar que una conexión no puede ser compartida simultáneamente con otros usuarios.

### **Pool con conexiones ocupadas**

En un pool de conexiones con 9 conexiones disponibles. Si todas las 9 conexiones están ocupadas y entra una décima requisición, ¿el usuario logra conectarse?

Rta.

El décimo usuario va a esperar que una de las 9 conexiones se libere.

### **Lo que aprendimos**

Lo que aprendimos en esta aula:

- Utilizar el pool de conexiones es una buena práctica;
- El pool de conexiones controla la cantidad de conexiones abiertas entre la aplicación y la base de datos;
  - Es normal que haya un mínimo y un máximo de conexiones.
- De la misma forma que hay, en JDBC, una interfaz para representar la conexión (java.sql.Connection), también hay una interfaz que representa el pool de conexiones (javax.sql.DataSource);
- C3P0 es una implementación Java de un pool de conexiones.

---

## **Capa de persistencia con DAO**

### **¿ Que es DAO ?**

Patrón Data Transfer Object (DTO), se trata de un objeto serializable para la transferencia de datos.

Patrón Data Access object (DAO), consiste en utilizar un objeto de acceso a datos para abstraer y encapsular todos los accesos a la fuente de datos. El DAO maneja la conexión con la fuente de datos para obtener y almacenar datos.

### **Ventajas del estándar DAO**

¿Cuáles son las ventajas de utilizar clases con el estándar DAO?

Rta.

Tiene que ver con la capacidad de aislar en un lugar centralizado, toda la lógica de acceso al repositorio de datos de la entidad. Así estaremos evitando duplicación de código y centralización de la lógica.

### **DAOs y los constructores**

Las clases DAO que creamos reciben la conexión en el constructor. Imaginemos que, en lugar de hacer así, tomemos una nueva conexión automáticamente en un constructor sin argumentos, como en el ejemplo:

```java
public class ProductoDAO {
    private final Connection con;
    ProductoDAO() {
        con = Database.getConnection();
    }
    // restante de la lógica del DAO aquí
}
```

¿Qué pasa si una tarea necesita acceder dos datos, como el ProductoDAO y el CategoriaDAO? ¿Cuál desventaja tenemos en este abordaje?

Rta.

No será posible utilizar la transacción. Como cada uno de los DAOs tiene una conexión distinta, ellos van a estar en distintas transacciones. Y por eso no será posible utilizar este recurso.

### **Lo que aprendimos**

Lo que aprendimos en esta aula:

- Para cada tabla del modelo tenemos una clase de dominio;
  - Para la tabla de producto tenemos una clase Producto asociada.
  - Los objetos del tipo Producto representan un registro de la tabla.
- Para acceder a la tabla vamos a utilizar el estándar llamado Data Access Object (DAO);
  - Para cada clase de dominio hay un DAO asociado. Por ejemplo, la clase Producto posee la clase ProductoDAO.
  - Todos los métodos JDBC relacionados al producto deben estar encapsulados en ProductoDAO.
- Una aplicación es escrita en capas;
  - Las capas más conocidas son las de view, controller, modelo y persistencia, que componen el estándar MVC.
- El flujo de una requisición entre las capas es el siguiente;

```txt
view <--> controller <--> persistencia
```

- En este curso utilizamos una aplicación con las views y los controllers ya creados y enfocamos en la capa de persistencia y modelo;
- No es una buena práctica dejar los detalles de implementación de una capa en otras que no tienen esta responsabilidad (por ejemplo la capa de controller lanzar una SQLException);
- Aquí estamos aprendiendo con una aplicación desktop embebida, pero hay otros tipos de aplicaciones con otros tipos de view, como html para aplicaciones web.

---

## **Evitando queries N + 1**

### **Ventajas de la clave foránea**

José quería agregar una categoría a los productos y resolvió crear en la tabla una nueva columna del tipo string, llamada categoria. Con eso fue posible informar la categoría del producto en el momento de su creación. ¿Por qué esta estratégia no es la mejor?

Rta.

Puede haber un problema para estandarizar la inserción de la categoría porque el campo sería libre para que los usuarios puedan escribir como quieran, generando muchas variaciones para una misma categoría. Eso perjudica la utilización de filtros por categoría. Como cada usuario iba a poder escribir libremente podrían haber diferentes formas en el nombre de las categorías. Podrían haber errores de tipeo o caracteres adicionales. Y eso dificulta el filtrado de los productos por ser casi imposible cubrir todos los escenarios.

### **¿ Por qué evitar queries N + 1 ?**

¿Cuál es el problema de la aplicación tener queries N + 1?

Rta.

Porque son utilizadas múltiples queries, aumentando la cantidad de acceso a la base de datos y, por consecuencia, empeorando la performance de la aplicación y del propio sistema de base de datos. Cuando las consultas son sencillas no hay problemas. Pero cuanto más complejidad van teniendo nuestras consultas hay la necesidad de buscar más informaciones de múltiples tablas, aumentando el acceso exponencialmente. Eso impacta gravemente la performance de la aplicación y del sistema de base de datos.

### **Informaciones relacionadas**

José creó una relación entre dos tablas utilizando la clave foránea. Pero ahora tiene una duda de como hacer para buscar las informaciones relacionadas de las dos tablas en su aplicación. ¿Qué podemos decir a José que haga para relacionar las dos tablas en una sola búsqueda?

Rta.

José debe utilizar el INNER JOIN. Con el INNER JOIN José va a lograr buscar las informaciones que están relacionadas entre las dos tablas.

### **Lo que aprendimos**

Lo que aprendimos en esta aula:

- Cuando tenemos una relación entre tables debemos tener cuidado para no crear el problema de queries N + 1;
  - N + 1 quiere decir que, para buscar los datos de una relación, es ejecutada una query y luego una más por cada relación.
  - Este tipo de problema puede generar problemas de performance en la aplicación y en la base de datos.
  - Este tipo de problema puede ser evitado utilizando join en la query SQL.
