# **Java Polimorfismo - Entendiendo herencia e interfaces**

## **Introduccion a herencia**

### **Recordando a los constructores**

De las siguientes alternativas, ¿cuáles son verdaderas acerca de los constructores en Java?

Rta.

- Los constructores por defecto (default) son aquellos que no reciben parámetros.
- El constructor es llamado en la inicialización del objeto.

### **Code smells**

¿Cuáles fueron los problemas presentados en la implementación de la clase Funcionario?

Nota: Esos problemas en el código o en el diseño también son llamados de Code Smells. Existe un libro famoso llamado Clean Code, del autor Robert C Martin, que se hizo famoso por mostrar el Code Smells y como resolverlo. El libro ya es un poco antiguo, pero vale la pena leerlo.

Pero ahora vamos a resolver el ejercicio :)

¿Cuáles fueron los problemas presentados?

Rta.

- Código no muy expresivo
- Código repetido
- Muchos if que no paran de crecer

### **Herencia en Java**

Vimos cómo extender una clase en Java, por ejemplo:

```java
class Gerente extends Funcionario { }
```

La clase Gerente, al extender la clase Funcionario.

A) Hereda todas las características de la Funcionario.
B) Hereda todo el comportamiento de clase Funcionario.
C) Instancia un Funcionario.
D) Es un Funcionario.

¿Cuáles afirmaciones son correctas?

Rta.

- A : La clase Gerente hereda todas las características de la clase Funcionario. Todos los atributos también forman parte de la clase Gerente.
- B : La clase Gerente hereda todo el comportamiento de la clase Funcionario. Significa que, todos los métodos públicos pueden ser utilizados en la clase Gerente.
- C : ¡ Incorrecto ! . Al heredar no estamos creando o instanciando un Funcionario. Para crear un objeto siempre debemos usar new.
- D : Al extender la clase Funcionario se puede decir que el Gerente es un Funcionario. Lo que significa que lo veremos con más detalles. ¡Espere!

### **Sintaxis correcta**

¿Cuál es la sintaxis correcta para extender una clase en Java?

Rta.

```java
class Carro extends Vehiculo { }
```

En el mundo Java se usa la palabra llave extends. Solo por curiosidad, todas las otras afirmaciones son ejemplos de herencia en otros lenguajes

Herencia en C#: class Carro : Vehiculo { }
Herencia en Ruby: class Carro < Vehiculo
Herencia en Python: class Carro (Vehiculo)

---

## **Super y reescrita de metodos**

### **Sobreescritura**

Vimos que la sobreescritura es un concepto importante de la herencia, porque permite redefinir un comportamiento previsto en la clase madre a través de la clase hija. Ahora vea la clase Vehiculo abajo.

```java
class Vehiculo {
    public  void encender() {
        // Alguna implementación
    }
}
```

Y la clase Hija Carro:

```java
class Carro extends Vehiculo {
    // ????
}
```

En lo que hemos aprendido hasta ahora, ¿cuál de los siguientes métodos insertado en el lugar de // ???? sobrescribe de forma correcta?

Rta.

```java
public void encender() {
    // implementación
}
```

Observe que el método posee la misma firma. Esto significa, una misma visibilidad, un mismo retorno, un mismo nombre y los mismo parámetros.

### **Visibilidad**

En relación con lo que ha aprendido hasta ahora, ¿cuál es el orden correcto de los modificadores de visibilidad, de menor a mayor visibilidad?

Rta.

```txt
private < protected < public

La palabra llave con menor visibilidad es private, después viene protected y después public.
private - solo visible dentro de la clase.
protected - visible dentro de la clase y también para las hijas.
public - visible en todo lugar.
También tenga en cuenta que protected está relacionado con la herencia.
```

### **Private x Protected**

¿Cuál es la diferencia entre private y protected?

Rta.

Solo la propia clase en sí ve atributos/métodos private, mientras que protected es visto por la propia clase más las clases hijas.

Atributos y métodos protected pueden ser vistos por su propia clase y sus hijas. Sin embargo, con private solo la clase en sí ve los atributos/métodos.

### **Dominando la herencia**

Sobre la herencia en Java, juzgue las siguientes declaraciones:

1. Una clase puede tener varias hijas, pero solo una madre.
2. Desde una instancia de una clase hija, podemos llamar a cualquier método público que haya sido declarado en la clase Madre.
3. En la clase hija, podemos escoger que heredar de la clase madre.
4. En el siguiente ejemplo, Perro también hereda todo de la clase Animal:

```java
class Animal {
    // atributos y métodos
}

class Mamifero extends Animal {
    // atributos y métodos
}

class Perro extends Mamifero {
    // atributos y métodos
}
```

¿Qué afirmaciones son correctas?

Rta.

Solo las afirmaciones 1, 2 e 4 son correctas. Se puede llamar a cualquier método de la clase madre. Una clase puede tener diversas “hijas y nietas” (que se heredan unos de otros) pero no podemos escoger lo que será heredado.

### **Para saber más: Sobrecarga**

Existe otro concepto en los lenguajes OO que se llama sobrecarga que es mucho más simple que la sobreescritura y no depende de la herencia.

Por ejemplo, en nuestra clase Gerente, imagina otro nuevo método autenticar que recibe además de la contraseña también el login:

```java
public class Gerente extends Funcionario {

    private int contraseña;

    public int getContraseña() {
        return contraseña;
    }

    public void setContraseña(int contraseña) {
        this.contraseña = contraseña;
    }

    public boolean autenticar(int contraseña) {
        if (this.contraseña == contraseña) {
            return true;
        } else {
            return false;
        }
    }

    // Nuevo método, recibiendo dos parámetros
    public boolean autenticar(String login, int contraseña) {
        // implementación omitida
    }

    // Otros métodos omitidos
}
```

Observe que hemos creado una nueva versión del método autenticar. Ahora tenemos dos métodos de autenticar en la misma clase que varían en el número o tipo de parámetros. Esto se llama sobrecarga de métodos.

La sobrecarga no tiene en cuenta la visibilidad o retorno del método, solo los parámetros y no depende de la herencia.

---

## **Entendiendo polimorfismo**

### **¿Qué es Polimorfismo?**

En el mundo orientado a objetos, el polimorfismo permite que:

Rta.

Las referencias de tipos de clases más genéricos referencian objetos más específicos.

```java
// Empleado clase generica, Gerente objetos especificos.
Empleado e = new Gerente();
```

### **¿Porque no funciona?**

Vea el código a continuación, que debe estar dentro del método main:

```java
Empleado e = new Gerente();
e.autenticar(1234);
```

Según lo que aprendió en clase, ¿por qué no se compiló el código?

Rta.

Porque la referencia e* es de tipo *Empleado e la clase Empleado no tiene el método autenticar. Quien define lo que podemos chamar es la referencia, que es del tipo Empleado, y es la clase Empleado el que realmente no tiene ese método.

### **¿Cuál es la salida?**

Dada la clase Vehiculo:

```java
public class Vehiculo {
    public void encender() {
        System.out.println("Encendiendo vehículo");
    }
}
```

La clase Carro:

```java
class Carro extends Vehiculo {
    public void encender() {
        System.out.println("Encendiendo Carro");
    }
}
```

Y la clase Moto:

```java
class Moto extends Vehiculo {
    public void encender() {
        System.out.println("Encendiendo Moto");
    }
}
```

Vea el código con el método main:

```java
public class Teste {

    public static void main(String[] args) {

        Vehiculo m = new Moto();
        m.encender();

        Vehiculo c = new Carro();
        c.encender();
    }

}
```

Cuando se ejecuta, ¿qué se imprimirá en la consola?

Rta.

```txt
Encendiendo Moto
Encendiendo Carro
```

Siempre será llamado el método más específico, primero el método de Moto, después de Carro.

### **Tipo de referencia**

Continuamos con el ejemplo Vehiculo, Moto y Carro:

```java
public class Vehiculo {
    public void encender() {
        System.out.println("Encendiendo vehiculo");
    }
}

public class Carro extends Vehiculo {
    public void encender() {
        System.out.println("Encendiendo Carro");
    }
}

public class Moto extends Vehiculo {
    public void encender() {
        System.out.println("Encendiendo Moto");
    }
}
```

Y vea el código casi completo:

```java
public class Teste {

    public static void main(String[] args) {

        ???? v = new Carro();
    }

}
```

¿Qué podemos insertar en lugar de ???? para compilar el código sin errores?

Rta.

- Carro. Correcto, siempre podemos usar el mismo tipo de referencia y objeto.
- Vehiculo. Correcto, pues el carro es un Vehiculo.

---

## **Herencia y uso de constructores**

### **Herencia de clases**

Con respecto a la herencia de clases, seleccione las afirmaciones verdaderas:

- Cuando una clase hereda de otra clase, también recibe sus métodos.
- Cuando una clase hereda de otra clase, también recibe sus constructores automáticamente. ¡Incorrecto! Ya que solo recibe sus métodos y atributos. Recuerda que no tienes herencia de los constructores.
- Cuando una clase hereda de otra clase, también recibe sus atributos. Heredamos los atributos (el objeto se crea en base a todos los atributos de la jerarquía).

### **Sobre el constructor**

Aprendimos que la construcción de un objeto se basa en su(s) constructor(es).

¿Cuál de las siguientes alternativas es correcta?

Rta.

El constructor por _default_ de java deja de existir a partir del momento que alguno es declarado en la clase. Tan pronto como creamos nuestro propio constructor, el constructor predeterminado (sin parámetros) deja de existir. Sin embargo, nada impide agregar explícitamente el constructor por _default_.

### **La anotación @Override**

En la última clase vimos sobre la anotación @Override. ¿Cuál es su propósito?

Rta.

Se utiliza para sobrescribir el método de la clase madre, lo que indica que se ha modificado el método original.

---

## **Clases y metodos abstractos**

En las clases abstractas no podemos crear instancias de esa clase directamente. Podemos hacerla de una clase hija. Ademas podemos hacer dentro de el que los metodos sean abstractos, y estos no se encuentran implementados, obliga a los hijos que heredan de esa clase abstracta a implementar dicho metodo.

### **Acerca de las clases abstractas**

¿Cuál de las siguientes afirmaciones es verdadera sobre las clases abstractas?

Rta.

No se pueden ser instanciadas. Para crear una instancia, primero debemos crear una clase hija no abstracta.

Una clase abstracta representa un concepto, algo abstracto, y el compilador no permite instanciar un objeto de esa clase. Para crear una instancia, es necesario crear primero una clase hija no abstracta.

¿Cuál de las siguientes afirmaciones es verdadera sobre los métodos abstractos?

Rta.

No tienen cuerpo (implementación), solo definen la firma. Un método abstracto define solo la firma (visibilidad, retorno, nombre del método y parámetros).

### **Clases y métodos abstractos**

Acerca de las clases y métodos abstractos, de las siguientes declaraciones, cuáles son verdaderas?

- Las clases abstractas son útiles cuando queremos utilizar comportamientos y atributos basados ​​en clases con comportamientos comunes.
- Usamos métodos abstractos cuando queremos "forzar" a un hijo concreto (clase concreta) a implementar un método. Ese es el significado de los métodos abstractos, garantizar que el hijo implemente un comportamiento.

### **¿Conoces las clases abstractas?**

¿Qué es cierto sobre las clases abstractas? Seleccione todas las declaraciones verdaderas:

- No se puede crear una instancia. Correcto, porque lo abstracto (la clase) no puede volverse concreto (objeto). Por lo tanto, no podemos instanciar objetos de una clase abstracta.
- Puede tener métodos abstractos (sin implementación). Correcto, como vimos, una clase abstracta puede tener métodos sin implementación. Por lo tanto, obligamos a un hijo a implementar el método.
- Pueden tener atributos. Correcto, podemos tener atributos Una clase abstracta es una clase normal, simplemente no puede instanciar y puede tener métodos abstractos. ¡El resto sigue siendo válido!
- Puede tener métodos concretos (con implementación). Correcto, como pueden tener atributos, ¡también pueden tener métodos concretos!

---

## **Interfaces**

En una interfaz todos sus metodos son abstractos (no tienen implementacion) y no tiene atributos.

### **Heredar de varias clases**

Vimos en la última clase que no hay herencia múltiple en Java. ¿Cómo podemos evitar la falta de ella?

Rta.

Podemos solucionar esta situación con el uso de interfaces de esta forma logramos polimorfismo sin herencia.

### **Conceptos de interfaz**

Con respecto a las interfaces, ¿cuál de las siguientes alternativas es la correcta?

Rta.

Es un contrato donde el que firma es responsable de implementar estos métodos (cumplir el contrato)

### **Clases abstractas x interfaces**

Acerca de las clases e interfaces abstractas, seleccione todas las declaraciones verdaderas:

- Podemos extender solo una clase abstracta, pero podemos implementar varias interfaces. Solo existe una herencia simple en Java, pero podemos implementar tantas interfaces como queramos.
- Todos los métodos de una interfaz son abstractos, los de una clase abstracta pueden no serlo. Todos los métodos en la interfaz son siempre abstractos y siempre públicos. En una clase abstracta podemos tener métodos concretos y abstractos.

### **Sobre el polimorfismo**

En cuanto al concepto de polimorfismo, marque las alternativas correctas:

- Es la capacidad de un objeto de ser referenciado por varios tipos. Podemos comunicarnos con un objeto a través de diferentes tipos de variables. Por ejemplo, si hay una clase Gerente que sea hija de Empleado. Un objeto de tipo Gerente puede ser referenciado como tipo Empleado también.
- Tenemos polimorfismo cuando una clase se extiende de otra o también cuando una clase implementa una interfaz. Tenemos polimorfismo por herencia o interfaz.

---

## **Practicando herencia e interfaz**

### **Revisión de conceptos de herencia**

Como vimos durante el curso y revisamos durante este capítulo, ¿cuál de las siguientes afirmaciones describe una ventaja de usar la herencia?

Rta.

La herencia captura lo que es común y aísla lo que es diferente entre clases.

### **Revisión de conceptos de interfaz**

Como vimos durante el curso y revisamos durante este capítulo, ¿cuál de las siguientes afirmaciones describe una ventaja de usar interfaces?

Rta.

Garantiza que todos los métodos de clase que implementan una interfaz se puedan llamar de forma segura. Esta es la idea del contrato, asegurar que la clase tenga un comportamiento, solo basta con firmar el contrato (implementar la interfaz).

### **Composición x Herencia**

¿Cuál de las siguientes afirmaciones representa una ventaja de utilizar la composición y las interfaces sobre el uso de la herencia?

Rta.

Con composiciones e interfaces tendremos más flexibilidad con nuestro código, ya que no estaremos apegados al acoplamiento que propone la herencia.
