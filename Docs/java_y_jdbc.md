## **Realice este curso para Java y:**

- Aprende a comunicarte con una base de datos relacional
- Conoceremos un poco mas de Statement y ResultSet
- Encapsular el acceso en un DAO
- Connection pool, dataSources y otros recursos importantes

### **Aulas**

- **Introducción a JDBC**

  - Presentación
  - Entorno y versiones
  - Preparando el ambiente
  - Como conectar una aplicación a una base de datos
  - API e Interfaces
  - Los primeros pasos con JDBC
  - Excepción cuando se recupera la conexión
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Ejecutando comandos SQL en Java**

  - Proyecto del aula anterior
  - Conociendo la view de la aplicación
  - Download del proyecto con las views
  - Listado con Statement #1
  - Listado con Statement #2
  - Connection, Statement y ResulSet
  - Creando la ConnectionFactory
  - El estándar Factory
  - INSERT con Statement
  - DELETE con Statement
  - Retorno del método execute()
  - Desafío: Modificando un registro con Statement
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Realizando mejoras en el proyecto**

  - Proyecto del aula anterior
  - Evitando SQL Injection utilizando PreparedStatement
  - Sobre el PreparedStatement
  - Desafío: Utilizando PreparedStatement en las demás operaciones
  - Migrando para el PreparedStatement
  - Tomando el control de la transacción
  - JDBC y transacciones
  - Manejando el commit y el rollback
  - Auto-Commit
  - Utilizando el try-with-resources
  - try-with-resources y el método close()
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Escalabilidad con pool de conexiones**

  - Proyecto del aula anterior
  - ¿Qué es un pool y un datasource?
  - Aplicación con cliente único
  - Creando un pool de conexiones
  - Aplicación con múltiples clientes
  - Probando el pool de conexiones
  - Pool con conexiones ocupadas
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Capa de persistencia con DAO**

  - Proyecto del aula anterior
  - Creando el modelo producto
  - DAO con INSERT del producto
  - Ventajas del estándar DAO
  - Operación de listado en el ProductoDAO
  - DAOs y los constructores
  - Desafío: Operaciones de modificación y exclusión en el ProductoDAO
  - El estándar MVC
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Evitando queries N + 1**

  - Proyecto del aula anterior
  - Relación entre tablas
  - Ventajas de la clave foránea
  - El modelo, el controller y el DAO de categorías
  - Relacionando el producto con la categoría en el registro
  - Queries N + 1
  - ¿Por qué evitar queries N + 1?
  - Utilizando INNER JOIN #1
  - Utilizando INNER JOIN #2
  - Informaciones relacionadas
  - Haga lo que hicimos en aula
  - Proyecto final
  - Lo que aprendimos
  - Conclusión
