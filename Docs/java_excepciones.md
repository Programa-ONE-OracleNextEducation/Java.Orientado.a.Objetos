## **Realice este curso para Java y:**

- Conozca la pila de ejecución
- Use el modo de depuración
- Comprenda el manejo de excepciones
- Cree tus propias excepciones marcadas y no marcadas
- Lanza excepciones para cambiar el flujo de tu programa

### **Aulas**

- **Pila de ejecucion**

  - Presentación
  - Preparando el ambiente
  - Pila de ejecución
  - Pila de ejecución - Gráfico
  - Salida del Stack
  - Sobre Stack
  - Debug
  - Stack de debugger
  - Fotografia del stack
  - Haga lo que hicimos en el aula: Pila de ejecución
  - ¿Qué aprendimos?

- **Tratamiento de excepciones**

  - Proyecto del aula anterior
  - Errores
  - Analizando excepciones
  - Acerca de las excepciones
  - Tipos de excepciones
  - Controlando errores por tipo
  - Flujo
  - Controlando errores
  - Multi-catch
  - Haga lo que hicimos en el aula: Tratamiento de excepciones
  - ¿Qué aprendimos?

- **Lanzando excepciones**

  - Proyecto del aula anterior
  - Overview
  - Resumiendo, el contenido
  - Lanzando excepciones
  - ¿Por qué no se lanzó la excepción?
  - Diferencias entre clases de excepciones
  - Sobre el lanzamiento de excepciones
  - Haga lo que hicimos en el aula: lanzando una excepción
  - ¿Qué aprendimos?

- **Checked y unchecked**

  - Proyecto del aula anterior
  - Estructura de excepciones
  - Creando mi propia excepción
  - ¡Miguel pide ayuda! #1
  - Excepciones y errores
  - Extendiendo una excepción
  - Checked y unchecked
  - ¡Miguel pide ayuda! #2
  - ¿Miguel entendió la clase?
  - Haga lo que hicimos en el aula: Checked e Unchecked
  - Opcional: Probando error
  - ¿Qué aprendimos?

- **Aplicando excepciones**

  - Proyecto del aula anterior
  - Overview: Checked y unchecked
  - ¿Cómo compilar?
  - Extendiendo una excepción
  - ¿Qué captura?
  - Retornando excepciones
  - ¿Son útiles las excepciones?
  - Controlando checked exceptions
  - ¿Como resolver?
  - Haga lo que hicimos en el aula: Catch genérico
  - Haga lo que hicimos en el aula: Creando la excepción
  - Para saber más: Nomenclatura
  - ¿Qué aprendimos?

- **Finally y try with resources**

  - Proyecto del aula anterior
  - Creando conexión
  - Errores en conexión
  - Palabras clave
  - Aplicando finally
  - Bloque Finally
  - Tratamiento de excepciones
  - Try with resources
  - Try con recurso
  - Haga lo que hicimos en el aula: Finally y try with resources
  - Para saber más: Excepciones padrones
  - Proyecto Final
  - ¿Qué aprendimos?
  - Conclusión
