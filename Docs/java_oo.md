## **Realice este curso para Java y:**

- Domine el paradigma de programación más usado en el mercado
- Entienda lo que son referencias y objetos
- Use atributos, métodos de la instancia y de la clase
- Aprenda a definir objetos a través de constructores
- Aprenda sobre encapsulamiento

### **Aulas**

- **El problema del paradigma procedimental**

  - Presentación
  - Introducción a orientación de objetos
  - Idea central del paradigma OO
  - Un poco de programación procedural
  - Sea bienvenido, Fulano
  - Lo que aprendimos

- **Introducción a Orientación a objetos**

  - Definición del proyecto
  - Características de los objetos
  - Clase cuenta
  - Definiendo tipos
  - Instancias parte 1
  - Establecer valor de atributo
  - Instancias parte 2
  - Referencias a objetos
  - Valores default
  - Referencias parte 1
  - Referencias parte 2
  - Haga lo que hicimos en el aula: Creando las primeras clases
  - Lo que aprendimos

- **Definiendo Comportamientos**

  - Proyecto del aula anterior
  - Creacion metodo
  - Métodos
  - Implementacion metodo
  - ¿Cómo llamar a un método?
  - Uso this
  - ¿Conoces this?
  - Metodo con retorno
  - Métodos validos
  - ¿Dónde usar this?
  - Referenciando objetos
  - Declaración de método
  - Haga lo que hicimos en el aula: Creando métodos
  - Lo que aprendimos

- **Composición de Objetos**

  - Proyecto del aula anterior
  - Referencia objetos
  - Extrayendo lo que es común
  - Valores null
  - Referencias inicializacion
  - Problema inesperado
  - Solucionando el problema en el código
  - Haga lo que hicimos en el aula: Referencias
  - Lo que aprendimos

- **Encapsulamiento y Visibilidad**

  - Proyecto del aula anterior
  - Escapsulamiento
  - Público x Privado
  - Getter
  - Setter
  - Creando Getters e Setters
  - Generando getter setter
  - Referencias
  - Ventajas de los atributos privados.
  - Para aprender más: Cuidado con el modelo anémico
  - Haga lo que hicimos en el aula: Creando Getters y Setters
  - Lo que aprendimos

- **Constructores y miembros estáticos**

  - Proyecto del aula anterior
  - Constructor
  - Utilizando constructores
  - Constructor personalizado
  - ¿Dónde está el error?
  - Variables estaticas
  - Miembros estáticos
  - ¿Por qué no suma?
  - Para obtener más información: reutilizar entre constructores
  - Haga lo que hicimos en el aula: Creando constructores y variables estáticas.
  - Proyecto Final
  - Lo que aprendimos
  - Conclusión
