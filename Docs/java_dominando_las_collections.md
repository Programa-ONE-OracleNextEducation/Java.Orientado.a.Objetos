## **Realice este curso para Java y:**

- Vaya más allá de las principales clases y métodos de las Collections
- Apliques buenas prácticas de OO en los relacionamentos
- No tengas más miedo de los antiguos Iterators
- Set o List, ¿cual utilizar?
- Equals o hashCode de una vez

### **Aulas**

- **Trabajando con ArrayList**

  - Presentación
  - Preparando el ambiente
  - Conociendo de ArrayList
  - Cómo adicionar elementos a un ArrayList
  - Formas de leer una ArrayList
  - Recorrer una lista de String
  - Métodos adicionales
  - Ordenar una lista de String
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Listas de Objetos**

  - Proyecto del aula anterior
  - Creando listas con Objetos
  - Adicionar un Objeto a una lista
  - Métodos adicionales
  - Ordenando listas
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Relacionando Listas de Objetos**

  - Proyecto del aula anterior
  - Conociendo la interface List
  - Tipos de List
  - Referencias y encapsulamento entre listas
  - Creando listas
  - Diferencias entre ArrayList y LinkedList
  - Diferencias entre un ArrayList y LinkedList
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Conociendo más de Listas**

  - Proyecto del aula anterior
  - Métodos tradicionales usando Collections y Streams
  - Métodos de la clase Collections
  - Otros métodos usando Collections y Streams
  - Métodos de la interfaz Stream
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Usando la interface Set**

  - Proyecto del aula anterior
  - Conociendo la interface Set
  - Implementaciones de la interfaz Set
  - Uso de la inferface Set
  - Ventaja de usar un Set
  - Ejemplos adicionales usando la interface Set
  - Métodos de la interfaz Set
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Métodos equals y hashCode**

  - Proyecto del aula anterior
  - Porque usar equals o hashCode
  - Métodos equals y hashCode
  - Ejemplos adicionales usando equals o hashCode
  - Métodos de la interfaz Set
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Otros tipos de Sets y Iterators**

  - Proyecto del aula anterior
  - Otros tipos de Sets y Iterators
  - Conociendo de Iterator
  - Cual collection usar
  - Conociendo de Iterator
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Maps**

  - Proyecto del aula anterior
  - Definición de mapas
  - Conociendo de Mapas
  - Ejemplos de mapas
  - Conociendo de Mapas
  - Haga lo que hicimos en aula
  - Proyecto Final
  - Lo que aprendimos
  - Conclusión
