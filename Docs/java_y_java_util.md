## **Realice este curso para Java y:**

- Aprenda los fundamentos de las colecciones Java
- Explora el poder y la flexibilidad de las listas
- Conoce como trabajar con ArrayList, LinkdedList o Vector
- Entiende como funcionan las lambda expressions
- Manipula y ordena las colecciones

### **Aulas**

- **Conociendo Arrays**

  - Presentación
  - Conociendo Arrays
  - Acerca de arrays
  - La sintaxis del array
  - Operaciones
  - ¿Cuántas referencias?
  - Objetos
  - Acceso al array
  - Haga lo que hicimos en clase: Trabajando con arrays
  - Forma literal
  - ¿Qué aprendimos?

- **Guardando Referencias**

  - Proyecto del aula anterior
  - Cast Objeto #1
  - Cast Objeto #2
  - Array de clientes
  - Cast explícito e implícito
  - Cast implícito y explícito de referencias
  - Cast posible e imposible
  - ¿Qué declaración?
  - Cuales casts
  - Acerca de ClassCastException
  - Metodo Main
  - Haga lo que hicimos en clase: Arrays
  - ¿Qué aprendimos?

- **ArrayList y Generics**

  - Proyecto del aula anterior
  - Operaciones Array
  - Guarda Cuentas
  - Haga lo que hicimos en clase: GuardadorDeCuentas
  - Guarda Referencias
  - Añadir referencia
  - Arraylist
  - Desventajas del array
  - Acerca de Arraylist
  - Acerca de ArrayList #2
  - Haga lo que hicimos en clase: ArrayList
  - Código heredado
  - Beneficios Generics (genéricos)
  - Haga lo que hicimos en clase: Generics
  - Otras formas de inicialización
  - ¿Qué aprendimos?

- **Equals y más listas**

  - Proyecto del aula anterior
  - Comparando Elementos
  - Sobreescribiendo Equals
  - Firma del método
  - Acerca de equals
  - Listas Anexadas
  - Acerca de las listas
  - LinkedList vs ArrayList
  - Haga lo que hicimos en clase: Equals
  - De Array para List
  - ¿Qué aprendimos?

- **Vector e Interfaz Collection**

  - Proyecto del aula anterior
  - Vectores
  - Vector, mi Vector!
  - Superclase Collection
  - ¿Dominas listas?
  - Acerca de las colecciones
  - Haga lo que hicimos en clase: vector y colección
  - ¿Qué aprendimos?

- **Las clases Wrappers**

  - Proyecto del aula anterior
  - Integer
  - Autoboxing
  - Código confuso
  - Parsing
  - Wrappers
  - ¿Qué primitivo?
  - ¿Conoces los wrappers?
  - El Wrapper Integer
  - Haga lo que hicimos en clase: Wrappers
  - ¿Qué aprendimos?

- **Ordenando Listas**

  - Proyecto del aula anterior
  - Ordenando Listas
  - Usando Wrappers
  - Orden Natural
  - Interfaces para ordenar
  - Orden natural
  - ¿Por qué no funciona?
  - Desafío de Collections
  - Ordenar Arrays
  - Haga lo que hicimos en clase: Ordenación de listas
  - Haga lo que hicimos en clase: Ordenar por String
  - Haga lo que hicimos en clase: Orden Natural
  - ¿Qué aprendimos?

- **Clases anónimas y Lambdas**

  - Proyecto del aula anterior
  - Clases anonimas
  - ¿Cual objeto?
  - ¿Qué sucede?
  - Lambdas
  - ¿Y si fuera lambda?
  - Foreach con lambda
  - El patron Iterator
  - Haga lo que hicimos en clase: Clases anónimas
  - Haga lo que hicimos en clase: Lambdas
  - Proyecto Final
  - Conclusion
