## **Realice este curso para Java y:**

- Use imports y organize tu código a través de packages
- Compreende el significado de todos los modificadores de visibilidade tratamento de exceciones
- Documenta y distribuye tu biblioteca
- Conozca java.lang
- Entenda la imutabilidad y la classe String
- Aprende para que existe la clase java.lang.Object

### **Aulas**

- **Organizando clases con paquetes**

  - Presentación
  - Usando Paquetes
  - Acerca de FQN
  - Acerca de Paquetes
  - La mejor opción de paquete
  - Actualizando Clases
  - Importando Clases
  - Estructura de Clases
  - Estructura correcta
  - ¿Dominas los paquetes?
  - Haga lo que hicimos en clase: Trabajando con paquetes
  - Lo que aprendimos

- **Todos los modificadores de acceso**

  - Proyecto del aula anterior
  - Modificadores de Acceso
  - Acerca de los modificadores
  - ¿Cuál modificador? #1
  - ¿Cuál modificador? #2
  - Haga lo que hicimos en clase: Modificadores de acceso
  - Lo que aprendimos

- **Distribución de código**

  - Proyecto del aula anterior
  - Documentación Javadoc
  - Generando Javadoc
  - ¿Qué comentario?
  - Sobre javadoc
  - Para saber más: Todas las etiquetas
  - Otras anotaciones
  - Archivos JAR
  - Usando Bibliotecas con JAR
  - JAR Ejecutables
  - ¿Qué es un JAR?
  - Ejercicio Javadoc
  - Haga lo que hicimos en clase: javadoc
  - Haga lo que hicimos en clase: JAR
  - Para saber más: Maven
  - Lo que aprendimos

- **El paquete java.lang**

  - Proyecto del aula anterior
  - Conociendo java.lang
  - Acerca de java.lang
  - Excepciones de java.lang
  - Explorando String
  - Metodos String
  - La clase String
  - ¿Cuál es el resultado?
  - La interfaz CharSequence
  - La clase StringBuilder
  - Haga lo que hicimos en clase: String
  - Lo que aprendimos

- **La clase Object**

  - Proyecto del aula anterior
  - Explorando System
  - Método toString()
  - Propósito de toString()
  - Haga lo que hicimos en clase: toString()
  - Clase Object
  - Acerca de Object
  - Entendiendo el código
  - Una duda mas
  - Conclusión
