## **Realice este curso para Java y:**

- ¿JVM? ¿JDK? ¿JRE? ¿Qué son esas siglas?
- Compilando y ejecutando código Java
- Aprenda a usar Eclipse
- Variables y control de flujo
- Conozca los principales tipos de Java

### **Aulas**

- **¿Qué es Java?**

  - Presentación
  - Plataforma Java
  - Beneficios de la JVM
  - ¿Qué sistemas podemos desarrollar?
  - ¿Bytecode vs EXE?
  - Para saber más: El nombre Bytecode
  - Lo que aprendimos

- **El primer programa**

  - Instalación de java
  - Versiones
  - JRE vs JDK
  - Para saber más: JVM vs JRE vs JDK
  - Compilando código
  - Entrada de aplicación
  - Sobre compilación y ejecución
  - Compilar y ejecutar
  - Haz lo que hicimos: Instalando el JDK
  - Haz lo que hicimos: Escribiendo nuestro primer código
  - Lo que aprendimos

- **Programando con Eclipse**

  - Haz lo que hicimos: Instala el IDE de Eclipse
  - Instalando eclipse
  - Sobre IDEs y editores
  - Eclipse Workspace
  - Usando eclipse
  - Proyecto Java
  - Acerca de View Navigator
  - Haz lo que hicimos: Ejecutar nuestro programa en Eclipse
  - Lo que aprendimos

- **Tipos y variables**

  - Tipo entero
  - Crear una variable numérica
  - Haz lo que hicimos: usando el tipo int
  - Tipo double
  - Operaciones entre números
  - Haz lo que hicimos: utilizando el tipo double
  - Conversiones
  - Imprimir texto y números
  - Haz lo que hicimos: algunas conversiones en Java
  - Para saber más: Type Casting
  - Lo que aprendimos

- **Trabajando con caracteres**

  - Proyecto del aula anterior
  - Caracteres y string
  - Declarando String y char
  - ¿Cuál será el resultado?
  - Variables y memoria
  - Concatenación de String y enteros
  - Haz lo que hicimos: practicando char y String
  - Lo que aprendimos

- **Practicando condicionales**

  - Proyecto del aula anterior
  - Test if
  - Trabajar con if
  - Haz lo que hicimos: el condicional if
  - Boolean
  - Tipo booleano
  - Operador lógico
  - Haz lo que hicimos: un poco más de if
  - Scope e inicialización
  - Declaración de la variable
  - Opcional: tasa con ifs
  - Haz lo que hicimos: alcance de las variables
  - Para saber más: el comando switch
  - Lo que aprendimos

- **Controlando flujo con Loops**

  - Proyecto del aula anterior
  - Ciclo while
  - Mientras tanto, el while...
  - Fijación del bucle while
  - Scope ciclos
  - Un error de compilación...
  - Ciclo for
  - Transformar de while a for
  - Haz lo que hicimos: bucles
  - Ciclos anexados
  - Ciclos break
  - Enfoque en el comando break
  - Ejercitar bucles anidados y rotos
  - Desafío opcional: múltiplos de 3
  - Desafío opcional: factorial
  - Haz lo que hicimos: bucles más profundos
  - Proyecto Final
  - Lo que aprendimos
  - ¿Y ahora?
  - Conclusión
