# **Java Orientado a Objetos**

## **Sobre Java**

Java es uno de los lenguajes de programación más utilizados en todo el mundo, según el famoso ranking de TIOBE. Es un lenguaje orientado a objetos, multiplataforma y bastante completo por permitir la creación de un programa único para uso en diversas plataformas. La plataforma Java ha ganado muchos mercados diferentes, desde aplicaciones de teléfonos, aplicaciones de escritorio en grandes empresas y gobiernos, pasando por el creciente mercado mobile y el reciente despegue de IoT (Internet of Things). De todas formas, el gran mercado de Java actualmente sigue siendo back-end en sistemas Web.

La formación Java Orientado a Objetos de Alura es una guia de aprendizaje completo para que inicies una carrera en programación Java. Aprenderás desde los fundamentos del lenguaje con buenas prácticas hasta el conocimiento esencial para un buen desarrollo orientado a objetos.

Esta formación forma parte del Programa ONE, una alianza entre Alura Latam y Oracle.

## **De quien vas a aprender**

- Diego Arguelles Rojas
- Álvaro Hernando Camacho Diaz
- Icaro Oliveira Florencio
- Gustavo Polar Sanchez

## **Paso a paso**

### **1. Plan de estudios**

El plan de estudio es el primer paso para ayudarte a organizar tu rutina de estudio.

Fue creado estratégicamente para guiarte en este nuevo camino de aprendizaje, ayudarte a estudiar en menos tiempo y aumentar tu rendimiento.

En este plan de estudios encontrarás el orden sugerido en el que debe hacer los cursos y el tiempo estimado en el que debe completar cada módulo. Con él podrás seguir el ritmo del programa ONE, aprovechar al máximo tus tiempos de estudio y cada ruta de aprendizaje.

El plan de estudio se realizó utilizando la herramienta Trello, la cual te permite gestionar cualquier tipo de proyecto y tareas, para utilizarla debes acceder al enlace, registrarte de forma gratuita y seguir las recomendaciones dejadas en el tablero de Trello para utilizar este plan de estudio.

[Trello](https://trello.com/b/H9lQuF2A/ruta-java-g3 "Trello").

### **2. Entender el lenguaje Java y orientación a objetos**

El primer paso es entender el lenguaje Java, que es la base de todo lo que haremos en esta formación. Si quieres aprender a **desarrollar un sitio web**, el servidor o el **back-end de una aplicación**, o hasta una aplicación **mobile** que utiliza Java, este contenido te ayudará en tu camino. Para ello es esencial desarrollar un conocimiento sólido en **orientación a objetos** y buenas prácticas de código. De este modo, aquí aprenderás desde recursos básicos del lenguaje, como **variables, controles de flujo y ciclos de repetición, hasta los principales conceptos del paradigma de orientación a objetos, como clases, herencia e interfaces** y como todo eso te ayudará en tu día a día en la programación.

[Java JRE y JDK: Compile y ejecute su primer programa](https://app.aluracursos.com/course/java-primeros-pasos "Java JRE y JDK: compile y ejecute su primer programa"). Contenido [aqui](./Docs/java_jre_y_jdk.md).

[Java OO: Entendiendo la orientacion a objetos](https://app.aluracursos.com/course/java-parte2-introduccion-orientada-objetos "Java OO: entendiendo la orientacion a objetos"). Contenido [aqui](./Docs/java_oo.md).

[POO: ¿Que es la programacion orientada a objetos?](https://www.aluracursos.com/blog/poo-que-es-la-programacion-orientada-a-objetos "Alura LATAM").

[Java Polimorfismo: Entendiendo herencia e interfaces](https://app.aluracursos.com/course/java-parte-3-entendiendo-herencia-interfaces "Java Polimorfismo: entendiendo herencia e interfaces"). Contenido [aqui](./Docs/java_polimorfismo.md).

[Simplificando tu codigo en java: Conoce los enum #AlulaMás](https://www.youtube.com/watch?v=EoPvlE85XAQ "YouTube").

[Creando anotaciones en java](https://www.aluracursos.com/blog/crear-anotaciones-en-java "Alura LATAM").

[Java Excepciones: Aprenda a crear, lanzar y controlar excepciones](https://app.aluracursos.com/course/java-excepciones "Java Excepciones: Aprenda a crear, lanzar y controlar excepciones"). Contenido [aqui](./Docs/java_excepciones.md).

### **3. Conocer las principales bibliotecas**

Una de las grandes ventajas de Java es su extensa librería **multiplataforma**. Después de conocer la **programación orientada a objetos, herencia, interfaces y excepciones en Java**, el siguiente paso es conocer sus principales paquetes, como **java.lang** y las famosas colecciones de **java.util**. Y así seguir sumergiéndote en lenguaje Java.

[Java y java.lang: Programe con la clase Object y String](https://app.aluracursos.com/course/java-lang-clase-object-string "Java y java.lang: Programe con la clase Object y String"). Contenido [aqui](./Docs/java_y_java_lang.md).

[Java y java.util: Colecciones, Wrappers y Lambda expressions](https://app.aluracursos.com/course/java-util-coleciones-wrappers-lambda-expressions "Java y java.util: Colecciones, Wrappers y Lambda expressions"). Contenido [aqui](./Docs/java_y_java_util.md).

[Java: Dominando las collections](https://app.aluracursos.com/course/java-dominando-collections "Java: Dominando las collections"). Contenido [aqui](./Docs/java_dominando_las_collections.md).

### **4. Aprendiendo sobre base de datos y JDBC**

Llegó la hora de aprender sobre **base de datos y la API de JDBC** pero ¿Por qué es importante aprender sobre estas herramientas?

En nuestras aplicaciones, muchas veces necesitamos almacenar informaciones y para eso, utilizamos las bases de datos. Estos conocimientos nos permiten hacer sistemas increíbles que permitirán gestionar los datos de los usuarios de manera segura.

[Las caracteristicas mas destacadas de Java 8 en adelante](https://www.aluracursos.com/blog/caracteristica-destacables-java8-delante?utm_source=gnarus&utm_medium=timeline "Alura LATAM").

[Bases de datos relacionales](https://www.aluracursos.com/blog/base-de-datos-relacional?utm_source=gnarus&utm_medium=timeline "Alura LATAM").

[Introduccion a SQL con MySQL: Manipule y consulte datos](https://app.aluracursos.com/course/introduccion-sql-mysql-manipule-consulte-datos "Introduccion a SQL con MySQL: Manipule y consulte datos"). Contenido [aqui](./Docs/introduccion_a_sql_con_mysql.md).

[Conociendo JDBC](https://www.aluracursos.com/blog/conociendo-el-jdbc "Alura LATAM").

[Java y JDBC: Trabajando con una Base de Datos](https://app.aluracursos.com/course/java-jdbc-trabajando-base-datos "Java y JDBC: Trabajando con una Base de Datos"). Contenido [aqui](./Docs/java_y_jdbc.md).

### **5. Diagnóstico de lo aprendido**

**Este paso es obligatorio** Cuéntanos sobre tus conocimientos adquiridos hasta el momento. Es un diagnóstico personal y por ello es muy importante que seas sincero con tus respuestas.

[Ruta java](https://grupocaelum.typeform.com/to/mj3FcKf2 "Ruta java").

### **6. Encara el Challenge ONE Java**

¿Qué son los Alura Challenges?

Es una forma de implementar el Challenge Based Learning o sea, aprendizaje basado en desafíos que Apple ayudó a crear. Es un mecanismo donde podrás comprometerte en la resolución de un problema para después investigar soluciones con cursos, contenido y charlas; ¡O incluso con el conocimiento que ya tienes! Finalmente vas a actuar y colocar tu proyecto en el aire. Todo esto mientras comentas y ayudas en proyectos de otros alumnos y alumnas

¡Bienvenido a nuestro Challenge ONE Java, dirigido a ti que estás comenzando tus estudios en el área de Desarrollo Back End con Java y necesitas esa ayuda para comenzar!. Encara los desafíos y agrégalos en tu portafolio de proyectos.

¡Manos a la obra!

[Challenger ONE - Java | Alura Cursos Online](https://www.aluracursos.com/challenges/oracle-one-java "Challenger ONE - Java | Alura Cursos Online").
